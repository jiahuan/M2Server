object FormMain: TFormMain
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'A3M2'#21830#19994#30331#38470#22120#37197#32622#22120' www.A3M2.com'
  ClientHeight = 354
  ClientWidth = 491
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  object pgc1: TPageControl
    Left = 10
    Top = 8
    Width = 473
    Height = 337
    ActivePage = TabSheet1
    TabOrder = 0
    object ts2: TTabSheet
      Caption = #37197#32622#30331#24405#22120#21015#34920
      ImageIndex = 1
      object pgc2: TPageControl
        Left = 12
        Top = 0
        Width = 449
        Height = 297
        ActivePage = ts5
        Style = tsFlatButtons
        TabOrder = 0
        object ts3: TTabSheet
          Caption = #26381#21153#22120#35774#32622
          object grp2: TGroupBox
            Left = 0
            Top = 8
            Width = 441
            Height = 49
            Caption = #20998#32452#35774#32622
            TabOrder = 0
            object lbl12: TLabel
              Left = 8
              Top = 21
              Width = 54
              Height = 12
              Caption = #20998#32452#21015#34920':'
            end
            object cbbServerGroup: TComboBox
              Left = 64
              Top = 18
              Width = 100
              Height = 20
              Style = csDropDownList
              ItemHeight = 12
              TabOrder = 0
            end
            object btnAddGroup: TButton
              Left = 294
              Top = 14
              Width = 60
              Height = 25
              Caption = #22686#21152#20998#32452
              TabOrder = 1
              OnClick = btnAddGroupClick
            end
            object btnDelGroup: TButton
              Left = 356
              Top = 14
              Width = 60
              Height = 25
              Caption = #21024#38500#20998#32452
              TabOrder = 2
              OnClick = btnAddGroupClick
            end
            object BtnMoveSrvUp: TButton
              Left = 170
              Top = 14
              Width = 60
              Height = 25
              Caption = #20998#32452#19978#31227
              TabOrder = 3
              OnClick = BtnMoveSrvUpClick
            end
            object BtnMoveSrvDown: TButton
              Left = 232
              Top = 14
              Width = 60
              Height = 25
              Caption = #20998#32452#19979#31227
              TabOrder = 4
              OnClick = BtnMoveSrvDownClick
            end
          end
          object grp3: TGroupBox
            Left = 0
            Top = 63
            Width = 441
            Height = 177
            Caption = #26381#21153#22120#21015#34920
            TabOrder = 1
            object lvServerList: TListView
              Left = 8
              Top = 16
              Width = 425
              Height = 121
              Columns = <
                item
                  Caption = #26381#21153#22120#32452
                  Width = 90
                end
                item
                  Caption = #26174#31034#21517#31216
                  Width = 90
                end
                item
                  Caption = #26381#21153#22120#21517#31216
                  Width = 80
                end
                item
                  Caption = #26381#21153#22120#22320#22336
                  Width = 105
                end
                item
                  Caption = #31471#21475
                  Width = 40
                end>
              GridLines = True
              ReadOnly = True
              RowSelect = True
              TabOrder = 0
              ViewStyle = vsReport
            end
            object btnAddServer: TButton
              Left = 40
              Top = 142
              Width = 60
              Height = 25
              Caption = #22686#21152'(&A)'
              TabOrder = 1
              OnClick = btnAddServerClick
            end
            object btnDelServer: TButton
              Left = 200
              Top = 142
              Width = 60
              Height = 25
              Caption = #21024#38500'(&D)'
              TabOrder = 2
              OnClick = btnAddServerClick
            end
            object btnEditServer: TButton
              Left = 120
              Top = 142
              Width = 60
              Height = 25
              Caption = #20462#25913'(&E)'
              TabOrder = 3
              OnClick = btnAddServerClick
            end
            object BtnGameUp: TButton
              Left = 280
              Top = 142
              Width = 60
              Height = 25
              Caption = #19978#31227
              TabOrder = 4
              OnClick = BtnGameUpClick
            end
            object BtnGameDown: TButton
              Left = 360
              Top = 142
              Width = 60
              Height = 25
              Caption = #19979#31227
              TabOrder = 5
              OnClick = BtnGameDownClick
            end
          end
        end
        object ts4: TTabSheet
          Caption = #32593#39029#35774#32622
          ImageIndex = 1
          object grp5: TGroupBox
            Left = 0
            Top = 183
            Width = 441
            Height = 64
            Caption = #28216#25103#20869#32593#39029#22320#22336
            TabOrder = 0
            object lbl16: TLabel
              Left = 8
              Top = 19
              Width = 66
              Height = 12
              Caption = #32852#31995'GM'#25353#25197':'
            end
            object lbl7: TLabel
              Left = 8
              Top = 43
              Width = 54
              Height = 12
              Caption = #20805#20540#25353#25197':'
            end
            object edtGMUrl: TEdit
              Left = 80
              Top = 14
              Width = 348
              Height = 20
              MaxLength = 255
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              Text = 'http://www.A3M2.com/advise/'
            end
            object edtPayUrl: TEdit
              Left = 80
              Top = 40
              Width = 345
              Height = 20
              MaxLength = 255
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
              Text = 'http://www.A3M2.com/payment/'
            end
          end
          object GroupBox2: TGroupBox
            Left = 0
            Top = 92
            Width = 441
            Height = 90
            Caption = '                 '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #23435#20307
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            object Label5: TLabel
              Left = 8
              Top = 20
              Width = 54
              Height = 12
              Caption = #27880#20876#22320#22336':'
            end
            object Label6: TLabel
              Left = 8
              Top = 44
              Width = 54
              Height = 12
              Caption = #20462#25913#23494#30721':'
            end
            object Label8: TLabel
              Left = 8
              Top = 68
              Width = 54
              Height = 12
              Caption = #25214#22238#23494#30721':'
            end
            object Label9: TLabel
              Left = 8
              Top = 0
              Width = 112
              Height = 12
              Caption = 'SQL'#29256#30456#20851#39029#38754#35774#32622
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -12
              Font.Name = #23435#20307
              Font.Style = [fsBold]
              ParentFont = False
            end
            object edtRegUrl: TEdit
              Left = 80
              Top = 16
              Width = 345
              Height = 20
              Hint = #24403#24320#21551'SQL'#27169#24335#21518','#27492#22788#26159#35774#32622#27880#20876#24080#21495#30340#30456#20851#22320#22336
              Enabled = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              Text = 'http://www.A3M2.com/i/'
            end
            object edtChangePassUrl: TEdit
              Left = 80
              Top = 40
              Width = 345
              Height = 20
              Hint = #24403#24320#21551'SQL'#27169#24335#21518','#27492#22788#26159#35774#32622#20462#25913#23494#30721#30340#30456#20851#22320#22336
              Enabled = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
              Text = 'http://www.A3M2.com/i/changepassword/'
            end
            object edtLostPassUrl: TEdit
              Left = 80
              Top = 64
              Width = 345
              Height = 20
              Hint = #24403#24320#21551'SQL'#27169#24335#21518','#27492#22788#26159#35774#32622#25214#22238#23494#30721#30340#30456#20851#22320#22336
              Enabled = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 2
              Text = 'http://www.A3M2.com/i/lostpassword/'
            end
          end
          object GroupBox3: TGroupBox
            Left = 0
            Top = 0
            Width = 441
            Height = 92
            Align = alTop
            Caption = 'DB'#29256#30456#20851#39029#38754#35774#32622
            TabOrder = 2
            object lbl8: TLabel
              Left = 8
              Top = 22
              Width = 66
              Height = 12
              Caption = #30331#24405#22120#31383#21475':'
            end
            object Label3: TLabel
              Left = 8
              Top = 48
              Width = 54
              Height = 12
              Caption = #23448#26041#32593#31449':'
            end
            object Label2: TLabel
              Left = 8
              Top = 70
              Width = 54
              Height = 12
              Caption = #20805#20540#20013#24515':'
            end
            object edtPayUrl2: TEdit
              Left = 80
              Top = 66
              Width = 345
              Height = 20
              MaxLength = 255
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              Text = 'http://www.A3M2.com/'
            end
            object edtLoginframeUrl: TEdit
              Left = 80
              Top = 19
              Width = 345
              Height = 20
              MaxLength = 255
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
              Text = 'http://www.A3M2.com/Login_frame/'
            end
            object edtHomeUrl: TEdit
              Left = 80
              Top = 43
              Width = 345
              Height = 20
              MaxLength = 255
              ParentShowHint = False
              ShowHint = True
              TabOrder = 2
              Text = 'http://www.A3M2.com/'
            end
          end
          object CheckBox1: TCheckBox
            Left = 0
            Top = 253
            Width = 97
            Height = 17
            Hint = #26159#21542#21551#29992'SQL'#23458#25143#31471#27169#24335
            Caption = #24320#21551'SQL'#29256
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -12
            Font.Name = #23435#20307
            Font.Style = [fsBold]
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
            OnClick = CheckBox1Click
          end
        end
        object ts5: TTabSheet
          Caption = #25968#25454#26356#26032#35774#32622
          ImageIndex = 2
          object grp7: TGroupBox
            Left = 0
            Top = 8
            Width = 441
            Height = 225
            Caption = #26356#26032#21015#34920
            TabOrder = 0
            object lvUpDataList: TListView
              Left = 8
              Top = 17
              Width = 426
              Height = 167
              Columns = <
                item
                  Caption = #26356#26032#25552#31034
                  Width = 100
                end
                item
                  Caption = #20445#23384#20301#32622
                  Width = 80
                end
                item
                  Caption = #20445#23384#25991#20214#21517
                  Width = 90
                end
                item
                  Caption = #19979#36733#22320#22336
                  Width = 150
                end
                item
                  Caption = #35299#21387
                  Width = 40
                end
                item
                  Caption = #26816#27979
                end
                item
                  Caption = #19979#36733
                end
                item
                  Caption = #26102#38388
                  Width = 100
                end
                item
                  Caption = #29256#26412#21495
                end
                item
                  Caption = 'MD5'
                  Width = 180
                end
                item
                  Caption = #19979#36733#25928#39564
                  Width = 180
                end>
              GridLines = True
              ReadOnly = True
              RowSelect = True
              TabOrder = 0
              ViewStyle = vsReport
            end
            object btnAddUp: TButton
              Left = 72
              Top = 190
              Width = 89
              Height = 25
              Caption = #22686#21152'(&A)'
              TabOrder = 1
              OnClick = btnAddUpClick
            end
            object btnDelUp: TButton
              Left = 293
              Top = 190
              Width = 89
              Height = 25
              Caption = #21024#38500'(&D)'
              TabOrder = 2
              OnClick = btnAddUpClick
            end
            object btnEditUp: TButton
              Left = 184
              Top = 190
              Width = 89
              Height = 25
              Caption = #20462#25913'(&E)'
              TabOrder = 3
              OnClick = btnAddUpClick
            end
          end
        end
      end
      object btnServerInfoSave: TButton
        Left = 109
        Top = 279
        Width = 113
        Height = 25
        Hint = #24314#35758#20445#23384#65292#26041#20415#19979#27425#26356#26032#20351#29992#12290
        Caption = #20445#23384#37197#32622#20449#24687'(&S)'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = btnServerInfoSaveClick
      end
      object btnServerInfoWrite: TButton
        Left = 252
        Top = 279
        Width = 113
        Height = 25
        Caption = #29983#25104#37197#32622#25991#20214'(&W)'
        TabOrder = 2
        OnClick = btnServerInfoWriteClick
      end
    end
    object TabSheet2: TTabSheet
      Caption = #23433#20840#35774#32622
      ImageIndex = 2
      object GroupBox4: TGroupBox
        Left = 17
        Top = 10
        Width = 438
        Height = 135
        Caption = #30331#24405#22120#23433#20840#35774#32622
        TabOrder = 0
        object Label11: TLabel
          Left = 8
          Top = 26
          Width = 84
          Height = 12
          Caption = #30331#38470#22120#29256#26412#21495#65306
        end
        object Label14: TLabel
          Left = 8
          Top = 52
          Width = 60
          Height = 12
          Caption = #32593#20851#23494#30721#65306
        end
        object Label12: TLabel
          Left = 8
          Top = 78
          Width = 96
          Height = 12
          Caption = #23458#25143#31471#22810#24320#38480#21046#65306
        end
        object editLoginBuildVer: TEdit
          Left = 110
          Top = 21
          Width = 299
          Height = 20
          Hint = #30331#38470#22120#29256#26412#21495
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Text = '20130430'
        end
        object editLoginPass: TEdit
          Left = 110
          Top = 47
          Width = 299
          Height = 20
          Hint = #23458#25143#31471#21644#32593#20851#36890#20449#23494#30721
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Text = 'PassWorld'
        end
        object editRunClientCount: TEdit
          Left = 110
          Top = 73
          Width = 121
          Height = 20
          TabOrder = 2
          Text = '1'
        end
      end
      object Button1: TButton
        Left = 109
        Top = 279
        Width = 113
        Height = 25
        Hint = #24314#35758#20445#23384#65292#26041#20415#19979#27425#26356#26032#20351#29992#12290
        Caption = #20445#23384#37197#32622#20449#24687'(&S)'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = btnServerInfoSaveClick
      end
      object Button2: TButton
        Left = 252
        Top = 279
        Width = 113
        Height = 25
        Caption = #29983#25104#37197#32622#25991#20214'(&W)'
        TabOrder = 2
        OnClick = btnServerInfoWriteClick
      end
    end
    object TabSheet1: TTabSheet
      Caption = #30331#24405#22120#29983#25104
      ImageIndex = 1
      object GroupBox1: TGroupBox
        Left = 16
        Top = 11
        Width = 438
        Height = 214
        Caption = #30331#24405#22120#37197#32622#35774#32622
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 26
          Width = 54
          Height = 12
          Caption = #21015#34920#22320#22336':'
        end
        object Label4: TLabel
          Left = 8
          Top = 131
          Width = 54
          Height = 12
          Caption = #28216#25103#21517#31216':'
        end
        object Label7: TLabel
          Left = 8
          Top = 79
          Width = 54
          Height = 12
          Caption = #30331#38470#25991#20214':'
        end
        object Label10: TLabel
          Left = 8
          Top = 53
          Width = 78
          Height = 12
          Caption = #30334#24230#21015#34920#22320#22336':'
        end
        object Label13: TLabel
          Left = 8
          Top = 105
          Width = 54
          Height = 12
          Caption = #32593#20851#25991#20214':'
        end
        object Edit1: TEdit
          Left = 86
          Top = 23
          Width = 339
          Height = 20
          Hint = #21015#34920#22320#22336#25903#25345#30334#24230#21015#34920#21644#36828#31243#21015#34920#65281
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Text = 'http://www.A3M2.com/ServerInfo.txt'
        end
        object Edit2: TEdit
          Left = 86
          Top = 127
          Width = 339
          Height = 20
          TabOrder = 1
          Text = 'A3M2'#30331#38470#22120'20130430'#29256
        end
        object RzButtonEdit2: TRzButtonEdit
          Left = 86
          Top = 75
          Width = 339
          Height = 20
          Text = '.\GameLogin_origin.exe'
          TabOrder = 2
          AltBtnWidth = 15
          ButtonWidth = 15
          OnButtonClick = RzButtonEdit2ButtonClick
        end
        object Edit3: TEdit
          Left = 86
          Top = 49
          Width = 339
          Height = 20
          Hint = #22791#29992#21015#34920#22320#22336#25903#25345#30334#24230#21015#34920#21644#36828#31243#21015#34920#65281
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          Text = 'http://www.A3M2.com/ServerInfo.txt'
        end
        object RzButtonEdit1: TRzButtonEdit
          Left = 86
          Top = 101
          Width = 339
          Height = 20
          Text = '.\LoginGate.exe'
          TabOrder = 4
          AltBtnWidth = 15
          ButtonWidth = 15
          OnButtonClick = RzButtonEdit1ButtonClick
        end
        object Button3: TButton
          Left = 86
          Top = 167
          Width = 133
          Height = 25
          Caption = #29983#25104#30331#38470#22120#21644#32593#20851'(W)'
          TabOrder = 5
          OnClick = Button3Click
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = #30456#20851#20449#24687
      ImageIndex = 3
      object GroupBox33: TGroupBox
        Left = 4
        Top = 8
        Width = 453
        Height = 154
        Caption = #29256#26412#20449#24687
        TabOrder = 0
        object LabelSoftName: TLabel
          Left = 8
          Top = 21
          Width = 223
          Height = 12
          AutoSize = False
          Caption = #31243#24207#21517#31216': A3M2'#30331#24405#22120#29983#25104#22120
        end
        object LabelSoftVersion: TLabel
          Left = 8
          Top = 41
          Width = 223
          Height = 12
          AutoSize = False
          Caption = #31243#24207#29256#26412': 1.00 '#29420#21019#29256
        end
        object LabelSoftDateTime: TLabel
          Left = 8
          Top = 61
          Width = 223
          Height = 12
          AutoSize = False
          Caption = #26356#26032#26085#26399': 2013/04/30'
        end
        object LabelSoftAuthor: TLabel
          Left = 8
          Top = 81
          Width = 223
          Height = 12
          AutoSize = False
          Caption = #31243#24207#20316#32773': A3M2'#24341#25806
        end
        object LabelSoftWWW: TLabel
          Left = 3
          Top = 101
          Width = 223
          Height = 12
          AutoSize = False
          Caption = #31243#24207#32593#31449': http://www.A3M2.com'
        end
        object LabelSoftBBS: TLabel
          Left = 3
          Top = 119
          Width = 223
          Height = 12
          AutoSize = False
          Caption = #31243#24207#35770#22363': http://www.A3M2com/bbs'
        end
      end
      object GroupBox34: TGroupBox
        Left = 3
        Top = 176
        Width = 453
        Height = 121
        Caption = #29256#26435#20844#21578
        TabOrder = 1
        object Label56: TLabel
          Left = 10
          Top = 24
          Width = 303
          Height = 94
          AutoSize = False
          Caption = 
            #26412#35745#31639#26426#31243#24207#21463#20013#21326#20154#27665#20849#21644#22269#30693#35782#20135#26435#19982#29256#26435#27861#20445#25252#65292#13#10#13#10#22914#26410#32463#25480#26435#32780#25797#33258#22797#21046#25110#20256#25773#26412#31243#24207'('#25110#20854#20013#20219#20309#37096#20221')'#65292#13#10#13#10#23558#21463#21040#20005#21385 +
            #30340#27665#20107#21450#21009#20107#21046#35009#65292#24182#22312#27861#24459#35768#21487#30340#33539#22260#20869#13#10#13#10#21463#21040#26368#22823#21487#33021#30340#36215#35785#12290
          Transparent = True
          WordWrap = True
        end
      end
    end
  end
  object xmldSetup: TXMLDocument
    Options = [doNodeAutoCreate, doNodeAutoIndent, doAttrNull, doAutoPrefix, doNamespaceDecl]
    Left = 384
    Top = 32
    DOMVendorDesc = 'MSXML'
  end
  object OpenDialog1: TOpenDialog
    Filter = #25351#23450'|Login.exe|'#25152#26377'|*.*'
    Title = #25171#24320
    Left = 412
    Top = 32
  end
  object xiaohu: TRSA
    CommonalityKey = '421166909'
    CommonalityMode = '78676586259407574469749356447'
    PrivateKey = '71362022359665638368808298557'
    Server = True
    Left = 440
    Top = 32
  end
end
