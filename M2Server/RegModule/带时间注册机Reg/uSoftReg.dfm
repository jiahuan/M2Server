object frmMain: TfrmMain
  Left = 207
  Top = 157
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = #27880#20876#26426'V11.00'
  ClientHeight = 440
  ClientWidth = 538
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label13: TLabel
    Left = 142
    Top = 311
    Width = 61
    Height = 13
    AutoSize = False
    BiDiMode = bdRightToLeft
    Caption = #26102#38388#38480#21046
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBiDiMode = False
    ParentFont = False
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 538
    Height = 137
    Align = alTop
    Caption = #27880#20876#20449#24687
    TabOrder = 0
    object Label2: TLabel
      Left = 24
      Top = 32
      Width = 72
      Height = 13
      Caption = #20351#29992#32773#21517#31216#65306
    end
    object Label3: TLabel
      Left = 24
      Top = 56
      Width = 72
      Height = 13
      Caption = #27880#20876#20154#22995#21517#65306
    end
    object Label4: TLabel
      Left = 24
      Top = 80
      Width = 48
      Height = 13
      Caption = #26657#39564#30721#65306
    end
    object Label12: TLabel
      Left = 24
      Top = 104
      Width = 48
      Height = 13
      Caption = #27880#20876#30721#65306
    end
    object Label14: TLabel
      Left = 288
      Top = 104
      Width = 72
      Height = 13
      Caption = #26412#26426#27880#20876#30721#65306
    end
    object Label15: TLabel
      Left = 288
      Top = 80
      Width = 72
      Height = 13
      Caption = #26412#26426#26657#39564#30721#65306
    end
    object edtPawnName: TEdit
      Left = 107
      Top = 32
      Width = 422
      Height = 21
      TabOrder = 0
      Text = #25105#30340#24341#25806#25105#20570#20027
    end
    object edtName: TEdit
      Left = 107
      Top = 56
      Width = 158
      Height = 21
      TabOrder = 1
      Text = #20992#21073#22914#26790
    end
    object edtCode: TEdit
      Left = 371
      Top = 104
      Width = 158
      Height = 21
      ReadOnly = True
      TabOrder = 2
    end
    object edtAuthCode: TEdit
      Left = 371
      Top = 80
      Width = 158
      Height = 21
      ReadOnly = True
      TabOrder = 3
    end
    object edtRegCode: TEdit
      Left = 107
      Top = 104
      Width = 158
      Height = 21
      TabOrder = 4
    end
    object edtAuthCode2: TEdit
      Left = 107
      Top = 80
      Width = 158
      Height = 21
      TabOrder = 5
    end
  end
  object btnGen: TButton
    Left = 138
    Top = 272
    Width = 81
    Height = 25
    Caption = #29983#25104#27880#20876#30721
    TabOrder = 1
    OnClick = btnGenClick
  end
  object btnReg: TButton
    Left = 226
    Top = 272
    Width = 81
    Height = 25
    Caption = #27880#20876
    TabOrder = 2
    OnClick = btnRegClick
  end
  object btnUnReg: TButton
    Left = 314
    Top = 272
    Width = 81
    Height = 25
    Caption = #21453#27880#20876
    TabOrder = 3
    OnClick = btnUnRegClick
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 144
    Width = 538
    Height = 119
    Caption = #26412#26426#27880#20876#20449#24687
    TabOrder = 4
    object Label1: TLabel
      Left = 24
      Top = 23
      Width = 60
      Height = 13
      Caption = #21097#20313#22825#25968#65306
    end
    object Label5: TLabel
      Left = 24
      Top = 47
      Width = 36
      Height = 13
      Caption = #36229#26399#65306
    end
    object Label6: TLabel
      Left = 290
      Top = 22
      Width = 60
      Height = 13
      Caption = #26159#21542#27880#20876#65306
    end
    object Label7: TLabel
      Left = 24
      Top = 71
      Width = 108
      Height = 13
      Caption = #26816#26597#26159#21542#26356#25913#26085#26399#65306
    end
    object Label8: TLabel
      Left = 24
      Top = 95
      Width = 84
      Height = 13
      Caption = #20801#35768#35797#29992#22825#25968#65306
    end
    object Label9: TLabel
      Left = 290
      Top = 46
      Width = 65
      Height = 13
      Caption = #21333#20301'/'#32452#32455#65306
    end
    object Label10: TLabel
      Left = 290
      Top = 70
      Width = 60
      Height = 13
      Caption = #27880#20876#22995#21517#65306
    end
    object Label11: TLabel
      Left = 290
      Top = 94
      Width = 48
      Height = 13
      Caption = #27880#20876#30721#65306
    end
    object edtRegistered: TEdit
      Left = 371
      Top = 16
      Width = 158
      Height = 21
      Enabled = False
      ReadOnly = True
      TabOrder = 0
    end
    object edtOrg2: TEdit
      Left = 371
      Top = 40
      Width = 158
      Height = 21
      Enabled = False
      ReadOnly = True
      TabOrder = 1
    end
    object edtName2: TEdit
      Left = 371
      Top = 64
      Width = 158
      Height = 21
      Enabled = False
      ReadOnly = True
      TabOrder = 2
    end
    object edtCode2: TEdit
      Left = 371
      Top = 88
      Width = 158
      Height = 21
      Enabled = False
      ReadOnly = True
      TabOrder = 3
    end
    object edtDaysLeft: TEdit
      Left = 142
      Top = 16
      Width = 122
      Height = 21
      Enabled = False
      ReadOnly = True
      TabOrder = 4
    end
    object edtExpired: TEdit
      Left = 142
      Top = 40
      Width = 122
      Height = 21
      Enabled = False
      ReadOnly = True
      TabOrder = 5
    end
    object edtCheckTamper: TEdit
      Left = 142
      Top = 64
      Width = 122
      Height = 21
      Enabled = False
      ReadOnly = True
      TabOrder = 6
    end
    object edtDays: TEdit
      Left = 142
      Top = 88
      Width = 122
      Height = 21
      Enabled = False
      ReadOnly = True
      TabOrder = 7
    end
  end
  object DateTimePicker1: TDateTimePicker
    Left = 209
    Top = 303
    Width = 186
    Height = 21
    Date = 41395.915203634260000000
    Time = 41395.915203634260000000
    TabOrder = 5
  end
  object RegwareIIII: TRegwareII
    MaxChars = 100
    ProgGUID = '{7FF02D23-3021-410E-88EB-57834625BB7F}'
    Seed = 123456789
    Left = 56
    Top = 272
  end
end
