获取当前系统日期和时间,ASP输出可以这样写：<%=now()%>

Year(now()) 获取年份， ASP输出：<%=Year(now())%>

Month(now()) 获取当前月份，ASP输出：<%=Month(now())%>

day(now()) 获取当天数，ASP输出：<%=day(now())%>

Minute(now()) 获取分钟数，ASP输出：<%=Minute(now())%>

Second(now()) 获取秒钟数，ASP输出：<%=Second(now())%>

date() 获取当前系统日期，格式为：2004-2-28

time() 获取当前系统时间，格式为：22:24:59

函数调用格式：FormatDateTime(Date[, NamedFormat])

FormatDateTime函数的语法有以下参数：

参数 描述

Date 必选，要被格式化的日期表达式。

NamedFormat 可选，指示所使用的日期/时间格式的数值，如果省略，则使用vbGeneralDate。

设置

NamedFormat参数可以有以下值：

vbGeneralDate 0 显示日期和/或时间；如果有日期部分，则将该部分显示为短日期格式；如果有时间部

分，则将该部分显示为长时间格式；如果都存在，则显示所有部分。

vbLongDate 1 使用计算机区域设置中指定的长日期格式显示日期。

vbShortDate 2 使用计算机区域设置中指定的短日期格式显示日期。

vbLongTime 3 使用计算机区域设置中指定的时间格式显示时间。

vbShortTime 4 使用24小时格式（hh:mm）显示时间。

示例：

<%

response.write formatdatetime(now,0)&"<br>"

response.write formatdatetime(now,1)&"<br>"

response.write formatdatetime(now,2)&"<br>"

response.write formatdatetime(now,3)&"<br>"

response.write formatdatetime(now,4)&"<br>"

%>

显示如下：

2007-9-5 1:52:27

2007年9月5日

2007-9-5

1:52:27

01:52  