unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls,StrUtils, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdHTTP;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Edit1: TEdit;
    DateTimePicker1: TDateTimePicker;
    Edit2: TEdit;
    Button2: TButton;
    IdHTTP1: TIdHTTP;
    Memo1: TMemo;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}
const
SEED_D=$45B7;
function GenerateCode(LicenseID: string; ExpireDate: TDate): string;
var
  S1, S2, S3, S4, S5, S6, S7, S8, S9, S10, S11, S12, S13, STMP: string;
  V1, V2, V3, I1, I2: integer;
  Month, Day, Year: Word;
begin
  Result := '';
  if (Length(LicenseID) > 50) or (Length(LicenseID) < 5) then Exit;

  //---- Encode Expiration Date ------------------------------------------
  if ExpireDate > 0 then
  begin
    DecodeDate(ExpireDate, Year, Month, Day);
  end else
  begin
    // Make expiration date of 0/00/0000 (no expiration)
    DecodeDate(0, Year, Month, Day);
  end;

  V1 := integer(Month) xor SEED_D and $000F;
  V2 := integer(Day)   xor SEED_D and $00FF;
  V3 := integer(Year)  xor SEED_D and $0FFF;

  S2 := IntToHex(V1, 1);  // Encoded month

  STMP := IntToHex(V2, 4);  // Encoded day
  S10 := STMP[3];
  S8 := STMP[4];

  STMP := IntToHex(V3, 4);  // Encoded year
  S4 := STMP[2];
  S6 := STMP[3];
  S12 := STMP[4];

  //---- Encode Segment 1 (chars #1,3,5) ---------------------------------
  V3 := Length(LicenseID);
  V1 := Ord(LicenseID[1]) + Ord(LicenseID[2]) + Ord(LicenseID[Trunc(V3 / 2)]) +
        Ord(LicenseID[V3]) + Ord(LicenseID[V3 - 1]);
  //V1 := V1 div 4;
  V2 := (188657688 mod V1) and $00FF;
  STMP := IntToHex(V2, 4);
  S1 := STMP[3];
  S3 := STMP[4];

  V1 := Length(LicenseID);
  if V1 > 16 then STMP := '0000' else STMP := IntToHex(V1, 4);
  S5 := STMP[4];

  //---- Encode Segment 2 (chars #7,9,11) --------------------------------
  V1 := 0;
  for I1 := 1 to Length(LicenseID) do
    V1 := V1 + Ord(LicenseID[I1]);
  V1 := ((V1 shl 4 xor 47006716) and $0FFF);
  STMP := IntToHex(V1, 4);
  S7 := STMP[2];
  S9 := STMP[3];
  S11 := STMP[4];

  //------------- Check segment 3 (chars #13...) ----------------------------
  V2 := 0;
  STMP := '';
  for I1 := 1 to Length(LicenseID) do
    V2 := V2 + Ord(LicenseID[I1]);
  V2 := V2 * ($7FFFFFF div V2);
  I2 := 31;
  for I1 := 1 to 32 do
  begin
    // Rotates the bits in FSeed3 through shifting,
    // but bits that fall off are moved to the other end
    V3 := (141819309 shl I1) or (141819309 shr I2) and $7FFFFFFF;
    V3 := abs(V3);
    if V3 > V2 then
      V1 := V3 mod V2
    else
      V1 := V2 mod V3;
    STMP := STMP + IntToHex($00000FFF and V1, 3);
    Dec(I2);
  end;
  // Take only the characters needed
  //Delete(STMP, RegCodeSize - 11, Length(STMP) - RegCodeSize - 11);
  S13 := Copy(STMP, 1, 20 - 12);

  Result := S1+S2+S3+S4+S5+S6+S7+S8+S9+S10+S11+S12+S13;
end;
function CheckCodeExpiration(RegCode: string): TDate;
var
  S1, S2, S3: string;
  I1, I2, I3: integer;
  Month, Day, Year: Word;
begin
    Result := 0;
    // The expiration date is encoded in characters 2, 4, 6, 8, 10, and 12
    S1 := RegCode[2];  // character #2 contains the month
    S2 := RegCode[10] + RegCode[8];  // characters #10 and #8 contain the day
    S3 := RegCode[4] + RegCode[6] + RegCode[12];  // chars #4, 6, 12 contain the year

    I1 := StrToIntDef('$' + S1, $FFFF);
    I2 := StrToIntDef('$' + S2, $FFFF);
    I3 := StrToIntDef('$' + S3, $FFFF);
    if (I1 or I2 or I3) = $FFFF then Exit;  // Failed on conversion

    Month :=   Word(I1 xor SEED_D and $000F);
    Day   :=   Word(I2 xor SEED_D and $00FF);
    Year  :=   Word(I3 xor SEED_D and $0FFF);

    Result := EncodeDate(Year, Month, Day);
end;

procedure TForm1.Button1Click(Sender: TObject);
var
ExpireDate: TDate;
s1,s2:string;
begin
ExpireDate:=DateTimePicker1.Date;
s1:=GenerateCode('4552524630',ExpireDate);
 ShowMessage(s1);
 ShowMessage(DateToStr(CheckCodeExpiration(s1)));
end;
    function HexToInt(hex:string):cardinal;  
    const cHex='0123456789ABCDEF';  
    var mult,i,loop:integer;  
    begin  
          result:=0;  
          mult:=1;  
          for loop:=length(hex)downto 1 do  
          begin
           i:=pos(hex[loop],cHex)-1;  
           if (i<0) then i:=0;  
           inc(result,(i*mult));
           mult:=mult*16;  
          end;  
    end;  
      
    function jiemi(const S:widestring):widestring;
    var  
      i: Integer;  
      FKey: Integer;  
      str:cardinal;  
      tmpstr:string;  
      len:integer;  
    begin  
          FKey :=  HexToInt(leftstr(s,2));  
          tmpstr:=rightstr(s,length(s)-2);
          len:=HexToInt(leftstr(tmpstr,2));
          tmpstr:=rightstr(tmpstr,length(tmpstr)-2);
          for i:=1 to Length(tmpstr) do  
          begin  
            str:=HexToInt(copy(tmpstr,0,2));  
            tmpstr:=rightstr(tmpstr,length(tmpstr)-2);
           Result := Result+Chr(Ord(str) xor fkey xor i);  
          end;  
          result:=leftstr(result,len);  
    end;

function SplitString(const source, ch: string): TStringList;
var
  temp, t2: string;
  i: integer;
begin
  result := TStringList.Create;
  temp := source;
  i := pos(ch, source);
  while i <> 0 do
  begin
    t2 := copy(temp, 0, i - 1);
    if (t2 <> '') then
      result.Add(t2);
    delete(temp, 1, i - 1 + Length(ch));
    i := pos(ch, temp);
  end;
  result.Add(temp);
end;
procedure SetDateTimeFormat();
var
p:DWORD;
begin
SetLocaleInfo(LOCALE_USER_DEFAULT,LOCALE_SSHORTDATE,pchar('yyyy-MM-dd')); //设置短日期格式
//SetLocaleInfo(LOCALE_USER_DEFAULT,LOCALE_SLONGDATE,pchar('yyyy''年''M''月 ''d''日'')); //设置长日期格式为 yyyy'年'M'月'd'日'，“年月日”字符必须用单引号括起来。Delphi字符串里必须用两个单引号。
SetLocaleInfo(LOCALE_USER_DEFAULT,LOCALE_STIMEFORMAT,pchar('H:mm:ss')); //设置时间格式，24小时制
SendMessageTimeOut(HWND_BROADCAST,WM_SETTINGCHANGE,0,0,SMTO_ABORTIFHUNG,10,p);//设置完成后必须调用，通知其他程序格式已经更改，否则即使是程序自身也不能使用新设置的格式
end;
procedure TForm1.Button2Click(Sender: TObject);
var
   stext:string;
   ss:TStringList;
begin
  Memo1.Clear;
  Edit2.Text:=string(jiemi(widestring(Edit2.Text)));
  stext:=IdHTTP1.Get('http://www.a3m2.com/timer.asp');
  ss:=SplitString(stext,'-');
  Memo1.Lines.Add(ss[0]);//年
  Memo1.Lines.Add(ss[1]);//月
  Memo1.Lines.Add(ss[2]);//日
  Memo1.Lines.Add(stext);
//  Memo1.Lines.Add(formatdatetime('yyyy-mm-dd',now));
  ss.Free;
end;

end.
