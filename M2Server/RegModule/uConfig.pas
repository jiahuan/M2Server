unit uConfig;

interface

uses
  Windows, SysUtils, Classes, IniFiles, YxdPersistent;

type
  /// <summary>
  /// 服务器配置
  /// </summary>
  TAppConfig = class(TYXDPersistent)
  private
    FUseDLVehicleDB: Boolean;
//    fSmsServiceUrl: string;
    FEditUserName: string;  //用户名：
    FEditRegisterCode:string;//注册码：
    FEditRegisterName:string;//机器码：
    GetMACAddress:string;//'网卡物理地址：'+ Info.GetMACAddress();
    GetIDEDiskSerialNumber:string;//'硬盘物理序列号：'+ Info.GetIDEDiskSerialNumber;
    GetIDEDiskDriveInfo:string;//'C：盘序列号：'+ Info.GetIDEDiskDriveInfo('C');
    GetCPUInfo1:string;//'CPU 序列号：'+ Info.GetCPUInfo(1);
    GetCPUInfo2:string;//'CPU 频率：'+ Info.GetCPUInfo(2);
    GetCPUInfo3:string;//'CPU 厂商：'+ Info.GetCPUInfo(3);}
    FLbsURL:string;
//    FStream: TMemoryStream;
    function GetLbsURL:string;
  public
    constructor Create;
    destructor Destroy; override;
  published
//    property Stream: TMemoryStream read FStream write FStream;
    property UseDLVehicleDB: Boolean read FUseDLVehicleDB write FUseDLVehicleDB default True;
    //-注册开始
    property UserName: string read FEditUserName write FEditUserName;
    property RegisterCode: string read FEditRegisterCode write FEditRegisterCode;
    property RegisterName: string read FEditRegisterName write FEditRegisterName;
    //-注册结束
    //-硬件信息开始
    property MACAddress: string read GetMACAddress write GetMACAddress;
    property IDEDiskSerialNumber: string read GetIDEDiskSerialNumber write GetIDEDiskSerialNumber;
    property IDEDiskDriveInfo: string read GetIDEDiskDriveInfo write GetIDEDiskDriveInfo;
    property CPUInfo1: string read GetCPUInfo1 write GetCPUInfo1;
    property CPUInfo2: string read GetCPUInfo2 write GetCPUInfo2;
    property CPUInfo3: string read GetCPUInfo3 write GetCPUInfo3;
    //-硬件信息结束
    property LBSURL:string read GetLbsURL write FLbsURL;
  end;

var
  /// <summary>
  /// 系统配置
  /// </summary>
  CConfig: TAppConfig;

implementation

{ TAppConfig }

constructor TAppConfig.Create;
begin
  inherited;
//  FStream := TMemoryStream.Create;
end;

destructor TAppConfig.Destroy;
begin
//  FreeAndNil(FStream);
  inherited;
end;

function TAppConfig.GetLbsURL: string;
begin
  if FLbsURL='' then
    Result:='http://www.baidu.com/'
  else
    Result :=FLbsURL;
end;



initialization
  CConfig := TAppConfig.Create;
  CConfig.LoadFromFile(ExtractFilePath(ParamStr(0)) + 'key.json', afJson);
finalization
  CConfig.SaveToFile(ExtractFilePath(ParamStr(0)) + 'key.json', afJson);
  FreeAndNil(CConfig);
end.

