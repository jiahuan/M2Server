{*******************************************************}
{                                                       }
{       对象序列化（持久化）基础库                      }
{                                                       }
{       版权所有 (C) 2013 GXT  YangYxd                  }
{                                                       }
{*******************************************************}

unit YxdPersistent;

interface

uses
  ActiveX, Windows, Classes, SysUtils, Variants, TypInfo, Controls, DB, IniFiles, 
  XMLIntf, XMLDoc, uJSON;
  
const
  PROP_NO_DEF_VALUE = Integer($80000000);
const
  CSTR_COLLECTION_TAG = 'Collection';
  CSTR_COLLECTION_ITEM_TAG = 'Item';

type
  /// <summary>
  /// 对象持久化存档格式
  /// </summary>
  TArchiveFormat = (
      afBinary,   {二进制数据}
      afXML,      {XML格式}
      afIni,      {ini文件}
      afJson      {json格式}
  );

type
  /// <summary>
  /// 对象持久化写入接口
  /// </summary>
  IYXDClassWriter = interface(IInterface)
    procedure SaveToStream(avOut: TStream);
    
    function BeginWriteRoot(const ClsName: string): Boolean;
    procedure EndWriteRoot;
    function BeginWrite(const Name: string): Boolean;
    procedure EndWrite;
    function BeginWriteList(ListKind: TTypeKind; ItemCount: Integer): Boolean;
    procedure EndWriteList;
    function BeginWriteListItem(Index: Integer): Boolean;
    procedure EndWriteListItem;

    procedure WriteChar(const Name: string; Value: Char);
    procedure WriteAnsiChar(const Name: string; Value: AnsiChar);
    procedure WriteWideChar(const Name: string; Value: WideChar);
    procedure WriteByte(const Name: string; Value: Byte);
    procedure WriteWord(const Name: string; Value: Word);
    procedure WriteInteger(const Name: string; Value: Integer);
    procedure WriteInt64(const Name: string; Value: Int64);
    procedure WriteFloat(const Name: string; Value: Extended);
    procedure WriteString(const Name: string; const Value: string);
    procedure WriteAnsiStr(const Name: string; const Value: AnsiString);
    procedure WriteWideStr(const Name: string; const Value: WideString);
    procedure WriteStrListItem(Index: Integer; const Value: string);
    procedure WriteAnsiStrListItem(Index: Integer; const Value: AnsiString);
    procedure WriteWideStrListItem(Index: Integer; const Value: WideString);
    procedure WritePointer(const Name: string; Buf: Pointer; Len: Integer);
  end;

type
  /// <summary>
  /// 对象持久化装载接口
  /// </summary>
  IYXDClassReader = interface(IInterface)
    procedure LoadFromStream(avIn: TStream);
    
    function BeginReadRoot(out ClsName: string): Boolean;
    function BeginRead(const Name: string): Boolean;
    procedure EndRead;
    function BeginReadList(out Count: Integer): Boolean;
    procedure EndReadList;
    function BeginReadListItem(Index: Integer): Boolean;
    function EndReadListItem: Boolean;

    function ReadChar(const Name: string; out Value: Char): Boolean;
    function ReadAnsiChar(const Name: string; out Value: AnsiChar): Boolean;
    function ReadWideChar(const Name: string; out Value: WideChar): Boolean;
    function ReadByte(const Name: string; out Value: Byte): Boolean;
    function ReadWord(const Name: string; out Value: Word): Boolean;
    function ReadInteger(const Name: string; out Value: Integer): Boolean;
    function ReadInt64(const Name: string; out Value: Int64): Boolean;
    function ReadFloat(const Name: string; out Value: Extended): Boolean;
    function ReadString(const Name: string; out Value: string): Boolean;
    function ReadAnsiStr(const Name: string; out Value: AnsiString): Boolean;
    function ReadWideStr(const Name: string; out Value: WideString): Boolean;
    function ReadStrListItem(Index: Integer; out Value: string; out More: Boolean): Boolean;
    function ReadAnsiStrListItem(Index: Integer; out Value: AnsiString; out More: Boolean): Boolean;
    function ReadWideStrListItem(Index: Integer; out Value: WideString; out More: Boolean): Boolean;
    function ReadPointer(const Name: string; Buf: Pointer; var Len: Integer): Boolean;
  end;

type
  /// <summary>
  /// 以RTTI方式持久化接口
  /// </summary>
  IYXDPersistent = interface(IInterface)
    procedure Assign(Source: TPersistent);
    function GetNamePath: string;
    procedure SaveToFile(const FileName: string; Format: TArchiveFormat);
    procedure LoadFromFile(const FileName: string; Format: TArchiveFormat);
    procedure SaveToStream(avOut: TStream; Format: TArchiveFormat);
    procedure LoadFromStream(avIn: TStream; Format: TArchiveFormat);
  end;

type
  /// <summary>
  /// RTTI对象支持列表
  /// </summary>
  TYXDPropList = class(TObject)
  private
    FInstance: TObject;
    FPropCount: Integer;
    FPropList: PPropList;
    procedure GetClassProp;
    procedure SetInstance(const Value: TObject);
  protected
    function GetPropName(Index: Integer ) : ShortString;
    function GetProp(Index: Integer): PPropInfo;
  public
    constructor Create(aInstance: TObject);
    destructor Destroy; override;
    procedure Clear; virtual;
    property PropCount: Integer read FPropCount;
    property PropNames[Index: Integer]: ShortString read GetPropName;
    property Props[Index: Integer]: PPropInfo read GetProp;
    property Instance: TObject read FInstance write SetInstance;
  End;

type
  /// <summary>
  /// RTTI方式持久化对象基类 (注：支持流TStream，TBytes序列化)
  /// </summary>
  TYXDPersistent = class(TPersistent, IYXDPersistent)
  private
    procedure ReadObject(Instance: TObject; const Name: string; const Reader: IYXDClassReader);
    procedure WriteObject(Instance: TObject; const Name: string; const Writer: IYXDClassWriter);
    function ReadVariant(const Name: string; const Reader: IYXDClassReader): Variant;
    // 注：当Variant为数组时，只处理TBytes字节数组
    procedure WriteVariant(const Value: Variant; const Name: string; TypeData: PTypeData; const Writer: IYXDClassWriter);
    function GetPropAsString(const Name: ShortString): string; overload;
    function GetPropAsString(Instance: TObject; Prop: PPropInfo): string; overload;
    function GetPropAsString(Instance: TObject; const PropName: string): string; overload;
    procedure SetPropAsString(const Name: ShortString; const value: string); overload;
    procedure SetPropAsString(Instance: TObject; const PropName: string;
      const value: string); overload;
    procedure SetPropAsString(Instance: TObject; Prop: PPropInfo;
      const value: string); overload;
  protected
    FRefCount: Integer;
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
    function GetClassInstance: TObject;
    procedure AssignObject(Src, Dest: TObject; notInherits: Boolean = False);
    procedure AssignTo(Dest: TPersistent); override;
    procedure Error(errorCode: TRuntimeError);
    procedure LoadObject(Instance: TObject; const Reader: IYXDClassReader);
    procedure SaveObject(Instance: TObject; const Writer: IYXDClassWriter);
    procedure AssignProperties(Src: TObject); dynamic;
    procedure ReadProperties(const Reader: IYXDClassReader); dynamic;
    procedure WriteProperties(const Writer: IYXDClassWriter); dynamic;
    function GetExtProp(const Name: ShortString): Variant; dynamic;
    procedure SetExtProp(const Name: ShortString; const value: Variant); dynamic;
    function DefineDateTimeFormat(Prop: PPropInfo): string; overload; dynamic;
    function DefaultDateTimeFormat(tinfo: PTypeInfo): string; overload;
  public
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
    class function NewInstance: TObject; override;
    class function GetReaderInterface(Sender: TObject; Format: TArchiveFormat): IYXDClassReader;
    class function GetWriterInterface(Sender: TObject; Format: TArchiveFormat): IYXDClassWriter;
    procedure SaveToFile(const FileName: string; Format: TArchiveFormat = afBinary); virtual;
    procedure LoadFromFile(const FileName: string; Format: TArchiveFormat = afBinary); virtual;
    procedure SaveToStream(avOut: TStream; Format: TArchiveFormat = afBinary); virtual;
    procedure LoadFromStream(avIn: TStream; Format: TArchiveFormat = afBinary); virtual;
    property RefCount: Integer read FRefCount;
    property PropAsString[const Name: ShortString]: string
      read GetPropAsString write SetPropAsString;
  end;

  TYXDPersistentClass = class of TYXDPersistent;

type
  /// <summary>
  /// RTTI方式持久化对象列表基类
  /// </summary>
  TYXDPersistentList = class(TYXDPersistent)
  private
    fItemClass: TYXDPersistentClass;
    fObjects: TList;
    function GetSize: Integer;
  protected
    procedure WriteProperties(const Writer: IYXDClassWriter); override;
    procedure ReadProperties(const Reader: IYXDClassReader); override;
    procedure AssignProperties(Src: TObject); override;
    function GetItem(Index: Integer): TYXDPersistent;
    procedure SetItem(Index: Integer; Item: TYXDPersistent);
    function CreateClass: TYXDPersistent; virtual;
  public
    constructor Create; overload;
    constructor Create(ItemCls: TYXDPersistentClass); reintroduce; overload;
    destructor Destroy; override;
    procedure Clear;
    function Add: TYXDPersistent;
    function IndexOf(Item: TYXDPersistent): Integer;
    procedure AddToList(Obj: TYXDPersistent);
    procedure Insert(Index: Integer; Item: TYXDPersistent);
    procedure Remove(Index: Integer);
    /// <summary>
    /// 提取对象（返回提取出的对象并从列表中移除）
    /// </summary>
    function Extract(Index: Integer): TYXDPersistent;
    procedure Exchange(Index1, Index2: Integer);
    procedure LoadFromDataSet(Dataset: TDataSet; num: Integer = -1; Reader: IYXDClassReader = nil);
    Property Count: Integer read GetSize;
    Property Items[Index: Integer]: TYXDPersistent read GetItem write SetItem; default;
    Property ItemClass: TYXDPersistentClass read fItemClass write fItemClass;
  end;

type
  /// <summary>
  /// ini 格式对象写入器
  /// </summary>
  TYXDIniWriter = class(TInterfacedObject, IYXDClassWriter)
  private
    Ini: TMemIniFile;
    Hierarchy: TStrings;
    SectionName: string;
  public
    constructor Create(aInstance: TObject);
    destructor Destroy; override;
    procedure SaveToStream(avOut: TStream);
    
    function BeginWriteRoot(const ClsName: string): Boolean;
    procedure EndWriteRoot;
    function BeginWrite(const Name: string): Boolean;
    procedure EndWrite;
    function BeginWriteList(ListKind: TTypeKind; ItemCount: Integer): Boolean;
    procedure EndWriteList;
    function BeginWriteListItem(Index: Integer): Boolean;
    procedure EndWriteListItem;

    procedure WriteChar(const Name: string; Value: Char);
    procedure WriteAnsiChar(const Name: string; Value: AnsiChar);
    procedure WriteWideChar(const Name: string; Value: WideChar);
    procedure WriteByte(const Name: string; Value: Byte);
    procedure WriteWord(const Name: string; Value: Word);
    procedure WriteInteger(const Name: string; Value: Integer);
    procedure WriteInt64(const Name: string; Value: Int64);
    procedure WriteFloat(const Name: string; Value: Extended);
    procedure WriteString(const Name: string; const Value: string);
    procedure WriteAnsiStr(const Name: string; const Value: AnsiString);
    procedure WriteWideStr(const Name: string; const Value: WideString);
    procedure WriteStrListItem(Index: Integer; const Value: string);
    procedure WriteAnsiStrListItem(Index: Integer; const Value: AnsiString);
    procedure WriteWideStrListItem(Index: Integer; const Value: WideString);
    procedure WritePointer(const Name: string; Buf: Pointer; Len: Integer);
  end;

type
  /// <summary>
  /// ini 格式对象装载器
  /// </summary>
  TYXDIniReader = class(TInterfacedObject, IYXDClassReader)
  private
    Ini: TMemIniFile;
    Hierarchy: TStrings;
    SectionName: string;
    function IniReadString(const ident: string; out value: string): Boolean;
  public
    constructor Create(aInstance: TObject);
    destructor Destroy; override;
    procedure LoadFromStream(avIn: TStream);
    
    function BeginReadRoot(out ClsName: string): Boolean;
    function BeginRead(const Name: string): Boolean;
    procedure EndRead;
    function BeginReadList(out Count: Integer): Boolean;
    procedure EndReadList;
    function BeginReadListItem(Index: Integer): Boolean;
    function EndReadListItem: Boolean;

    function ReadChar(const Name: string; out Value: Char): Boolean;
    function ReadAnsiChar(const Name: string; out Value: AnsiChar): Boolean;
    function ReadWideChar(const Name: string; out Value: WideChar): Boolean;
    function ReadByte(const Name: string; out Value: Byte): Boolean;
    function ReadWord(const Name: string; out Value: Word): Boolean;
    function ReadInteger(const Name: string; out Value: Integer): Boolean;
    function ReadInt64(const Name: string; out Value: Int64): Boolean;
    function ReadFloat(const Name: string; out Value: Extended): Boolean;
    function ReadString(const Name: string; out Value: string): Boolean;
    function ReadAnsiStr(const Name: string; out Value: AnsiString): Boolean;
    function ReadWideStr(const Name: string; out Value: WideString): Boolean;
    function ReadStrListItem(Index: Integer; out Value: string; out More: Boolean): Boolean;
    function ReadAnsiStrListItem(Index: Integer; out Value: AnsiString; out More: Boolean): Boolean;
    function ReadWideStrListItem(Index: Integer; out Value: WideString; out More: Boolean): Boolean;
    function ReadPointer(const Name: string; Buf: Pointer; var Len: Integer): Boolean;
  end;

type
  /// <summary>
  /// 内存流 格式对象写入器
  /// </summary>
  TYXDBinaryWriter = class(TInterfacedObject, IYXDClassWriter)
  private 
    Mem: TMemoryStream;
  protected
    procedure WriteWord(avOut: TStream; avData: Word); overload; virtual;
    procedure WriteInteger(avOut: TStream; avData: Integer); overload; virtual;
  public
    constructor Create(aInstance: TObject);
    destructor Destroy; override;
    procedure SaveToStream(avOut: TStream);
    
    function BeginWriteRoot(const ClsName: string): Boolean;
    procedure EndWriteRoot;
    function BeginWrite(const Name: string): Boolean;
    procedure EndWrite;
    function BeginWriteList(ListKind: TTypeKind; ItemCount: Integer): Boolean;
    procedure EndWriteList;
    function BeginWriteListItem(Index: Integer): Boolean;
    procedure EndWriteListItem;

    procedure WriteChar(const Name: string; Value: Char);
    procedure WriteAnsiChar(const Name: string; Value: AnsiChar);
    procedure WriteWideChar(const Name: string; Value: WideChar);
    procedure WriteByte(const Name: string; Value: Byte);
    procedure WriteWord(const Name: string; Value: Word); overload;
    procedure WriteInteger(const Name: string; Value: Integer); overload;
    procedure WriteInt64(const Name: string; Value: Int64);
    procedure WriteFloat(const Name: string; Value: Extended);
    procedure WriteString(const Name: string; const Value: string);
    procedure WriteAnsiStr(const Name: string; const Value: AnsiString);
    procedure WriteWideStr(const Name: string; const Value: WideString);
    procedure WriteStrListItem(Index: Integer; const Value: string);
    procedure WriteAnsiStrListItem(Index: Integer; const Value: AnsiString);
    procedure WriteWideStrListItem(Index: Integer; const Value: WideString);
    procedure WritePointer(const Name: string; Buf: Pointer; Len: Integer);
  end;

type
  /// <summary>
  /// 内存流 格式对象装载器
  /// </summary>
  TYXDBinaryReader = class(TInterfacedObject, IYXDClassReader)
  private
    Mem: TMemoryStream;
  protected
    function ReadWord(avIn: TStream): Word; overload; virtual;
    function ReadInteger(avIn: TStream): Integer; overload; virtual;
  public
    constructor Create(aInstance: TObject);
    destructor Destroy; override;
    procedure LoadFromStream(avIn: TStream);
    
    function BeginReadRoot(out ClsName: string): Boolean;
    function BeginRead(const Name: string): Boolean;
    procedure EndRead;
    function BeginReadList(out Count: Integer): Boolean;
    procedure EndReadList;
    function BeginReadListItem(Index: Integer): Boolean;
    function EndReadListItem: Boolean;

    function ReadChar(const Name: string; out Value: Char): Boolean;
    function ReadAnsiChar(const Name: string; out Value: AnsiChar): Boolean;
    function ReadWideChar(const Name: string; out Value: WideChar): Boolean;
    function ReadByte(const Name: string; out Value: Byte): Boolean;
    function ReadWord(const Name: string; out Value: Word): Boolean; overload;
    function ReadInteger(const Name: string; out Value: Integer): Boolean; overload;
    function ReadInt64(const Name: string; out Value: Int64): Boolean;
    function ReadFloat(const Name: string; out Value: Extended): Boolean;
    function ReadString(const Name: string; out Value: string): Boolean;
    function ReadAnsiStr(const Name: string; out Value: AnsiString): Boolean;
    function ReadWideStr(const Name: string; out Value: WideString): Boolean;
    function ReadStrListItem(Index: Integer; out Value: string; out More: Boolean): Boolean;
    function ReadAnsiStrListItem(Index: Integer; out Value: AnsiString; out More: Boolean): Boolean;
    function ReadWideStrListItem(Index: Integer; out Value: WideString; out More: Boolean): Boolean;
    function ReadPointer(const Name: string; Buf: Pointer; var Len: Integer): Boolean;
  end;

type
  /// <summary>
  /// XML 格式对象写入器
  /// </summary>
  TYXDXMLWriter = class(TInterfacedObject, IYXDClassWriter)
  private
    fDocument: IXMLDocument;
    fRootObject: IXMLNode;
  protected
    procedure SetObjectNode(const Value: IXMLNode);
  public
    constructor Create(aInstance: TObject);
    destructor Destroy; override;
    procedure SaveToStream(avOut: TStream);
    
    function BeginWriteRoot(const ClsName: string): Boolean;
    procedure EndWriteRoot;
    function BeginWrite(const Name: string): Boolean;
    procedure EndWrite;
    function BeginWriteList(ListKind: TTypeKind; ItemCount: Integer): Boolean;
    procedure EndWriteList;
    function BeginWriteListItem(Index: Integer): Boolean;
    procedure EndWriteListItem;

    procedure WriteChar(const Name: string; Value: Char);
    procedure WriteAnsiChar(const Name: string; Value: AnsiChar);
    procedure WriteWideChar(const Name: string; Value: WideChar);
    procedure WriteByte(const Name: string; Value: Byte);
    procedure WriteWord(const Name: string; Value: Word); overload;
    procedure WriteInteger(const Name: string; Value: Integer); overload;
    procedure WriteInt64(const Name: string; Value: Int64);
    procedure WriteFloat(const Name: string; Value: Extended);
    procedure WriteString(const Name: string; const Value: string);
    procedure WriteAnsiStr(const Name: string; const Value: AnsiString);
    procedure WriteWideStr(const Name: string; const Value: WideString);
    procedure WriteStrListItem(Index: Integer; const Value: string);
    procedure WriteAnsiStrListItem(Index: Integer; const Value: AnsiString);
    procedure WriteWideStrListItem(Index: Integer; const Value: WideString);
    procedure WritePointer(const Name: string; Buf: Pointer; Len: Integer);
  end;

type
  /// <summary>
  /// XML 格式对象装载器
  /// </summary>
  TYXDXMLReader = class(TInterfacedObject, IYXDClassReader)
  private
    fDocument: IXMLDocument;
    fRootObject: IXMLNode;
  protected
    procedure SetObjectNode(const Value: IXMLNode);
  public
    constructor Create(); reintroduce; overload;
    constructor Create(const Node: IXMLNode); reintroduce; overload;
    destructor Destroy; override;
    procedure LoadFromStream(avIn: TStream);
    
    function BeginReadRoot(out ClsName: string): Boolean;
    function BeginRead(const Name: string): Boolean;
    procedure EndRead;
    function BeginReadList(out Count: Integer): Boolean;
    procedure EndReadList;
    function BeginReadListItem(Index: Integer): Boolean;
    function EndReadListItem: Boolean;

    function ReadChar(const Name: string; out Value: Char): Boolean;
    function ReadAnsiChar(const Name: string; out Value: AnsiChar): Boolean;
    function ReadWideChar(const Name: string; out Value: WideChar): Boolean;
    function ReadByte(const Name: string; out Value: Byte): Boolean;
    function ReadWord(const Name: string; out Value: Word): Boolean; overload;
    function ReadInteger(const Name: string; out Value: Integer): Boolean; overload;
    function ReadInt64(const Name: string; out Value: Int64): Boolean;
    function ReadFloat(const Name: string; out Value: Extended): Boolean;
    function ReadString(const Name: string; out Value: string): Boolean;
    function ReadAnsiStr(const Name: string; out Value: AnsiString): Boolean;
    function ReadWideStr(const Name: string; out Value: WideString): Boolean;
    function ReadStrListItem(Index: Integer; out Value: string; out More: Boolean): Boolean;
    function ReadAnsiStrListItem(Index: Integer; out Value: AnsiString; out More: Boolean): Boolean;
    function ReadWideStrListItem(Index: Integer; out Value: WideString; out More: Boolean): Boolean;
    function ReadPointer(const Name: string; Buf: Pointer; var Len: Integer): Boolean;
  end;

type
  /// <summary>
  /// JSON 格式对象写入器
  /// </summary>
  TYXDJsonWriter = class(TInterfacedObject, IYXDClassWriter)
  private 
    json: TJSONObject;
    RootObject: TJSONObject;
  public
    constructor Create(aInstance: TObject);
    destructor Destroy; override;
    procedure SaveToStream(avOut: TStream);
    
    function BeginWriteRoot(const ClsName: string): Boolean;
    procedure EndWriteRoot;
    function BeginWrite(const Name: string): Boolean;
    procedure EndWrite;
    function BeginWriteList(ListKind: TTypeKind; ItemCount: Integer): Boolean;
    procedure EndWriteList;
    function BeginWriteListItem(Index: Integer): Boolean;
    procedure EndWriteListItem;

    procedure WriteChar(const Name: string; Value: Char);
    procedure WriteAnsiChar(const Name: string; Value: AnsiChar);
    procedure WriteWideChar(const Name: string; Value: WideChar);
    procedure WriteByte(const Name: string; Value: Byte);
    procedure WriteWord(const Name: string; Value: Word); overload;
    procedure WriteInteger(const Name: string; Value: Integer); overload;
    procedure WriteInt64(const Name: string; Value: Int64);
    procedure WriteFloat(const Name: string; Value: Extended);
    procedure WriteString(const Name: string; const Value: string);
    procedure WriteAnsiStr(const Name: string; const Value: AnsiString);
    procedure WriteWideStr(const Name: string; const Value: WideString);
    procedure WriteStrListItem(Index: Integer; const Value: string);
    procedure WriteAnsiStrListItem(Index: Integer; const Value: AnsiString);
    procedure WriteWideStrListItem(Index: Integer; const Value: WideString);
    procedure WritePointer(const Name: string; Buf: Pointer; Len: Integer);
  end;

type
  /// <summary>
  /// Json 格式对象装载器
  /// </summary>
  TYXDJsonReader = class(TInterfacedObject, IYXDClassReader)
  private
    json: TJSONObject;
    RootObject: TJSONObject;
    ref: Integer;
  public
    constructor Create(aInstance: TObject);
    destructor Destroy; override;
    procedure LoadFromStream(avIn: TStream);
    
    function BeginReadRoot(out ClsName: string): Boolean;
    function BeginRead(const Name: string): Boolean;
    procedure EndRead;
    function BeginReadList(out Count: Integer): Boolean;
    procedure EndReadList;
    function BeginReadListItem(Index: Integer): Boolean;
    function EndReadListItem: Boolean;

    function ReadChar(const Name: string; out Value: Char): Boolean;
    function ReadAnsiChar(const Name: string; out Value: AnsiChar): Boolean;
    function ReadWideChar(const Name: string; out Value: WideChar): Boolean;
    function ReadByte(const Name: string; out Value: Byte): Boolean;
    function ReadWord(const Name: string; out Value: Word): Boolean; overload;
    function ReadInteger(const Name: string; out Value: Integer): Boolean; overload;
    function ReadInt64(const Name: string; out Value: Int64): Boolean;
    function ReadFloat(const Name: string; out Value: Extended): Boolean;
    function ReadString(const Name: string; out Value: string): Boolean;
    function ReadAnsiStr(const Name: string; out Value: AnsiString): Boolean;
    function ReadWideStr(const Name: string; out Value: WideString): Boolean;
    function ReadStrListItem(Index: Integer; out Value: string; out More: Boolean): Boolean;
    function ReadAnsiStrListItem(Index: Integer; out Value: AnsiString; out More: Boolean): Boolean;
    function ReadWideStrListItem(Index: Integer; out Value: WideString; out More: Boolean): Boolean;
    function ReadPointer(const Name: string; Buf: Pointer; var Len: Integer): Boolean;
  end;   

function Base64Encode(Input: Pointer; InputSize: Integer;
  Output: Pointer): Integer; overload;
function Base64Encode(Input: Pointer; InputSize: Integer): AnsiString; overload;
function Base64Encode(const Input: AnsiString): AnsiString; overload;
function Base64Decode(Input: Pointer; InputSize: Integer;
  Output: Pointer): Integer; overload;
function Base64Decode(const Input: AnsiString;
  Output: Pointer): Integer; overload;
function Base64Decode(const Input: AnsiString): AnsiString; overload;

  
implementation

{ TYXDPropList }

procedure TYXDPropList.Clear;
begin
  if Assigned(FPropList) then
    FreeMem(FPropList);
  FPropCount := 0;
  FPropList := nil;
end;

constructor TYXDPropList.Create(aInstance: TObject);
begin
  FInstance := aInstance;
  GetClassProp;
end;

destructor TYXDPropList.Destroy;
begin
  Clear;
  inherited;
end;

procedure TYXDPropList.GetClassProp;
var
  TypeData: PTypeData;
begin
  Clear;
  if Assigned(FInstance) then begin
    if FInstance.ClassInfo = nil then Exit;  
    TypeData := GetTypeData(FInstance.ClassInfo);
    if TypeData = nil then Exit;    
    FPropCount := TypeData^.PropCount;
    if FPropCount > 0 then begin
      GetMem(FPropList, FPropCount * SizeOf(Pointer));
      GetPropInfos(FInstance.ClassInfo, FPropList);
    end;
  end;
end;

function TYXDPropList.GetProp(Index: Integer): PPropInfo;
begin
  if (Assigned(FPropList)) Then
    Result := FPropList[Index]
  else Result := nil;
end;

function TYXDPropList.GetPropName(Index: Integer): ShortString;
begin
  Result := GetProp(Index)^.Name;
end;

procedure TYXDPropList.SetInstance(const Value: TObject);
begin
  if FInstance <> Value then begin
    FInstance := Value;
    GetClassProp;
  end;
end;

{ TYXDPersistent }  

class function TYXDPersistent.GetReaderInterface(Sender: TObject;
  Format: TArchiveFormat): IYXDClassReader;
begin
  case Format of
    afBinary: Result := TYXDBinaryReader.Create(Sender);
    afXML: Result := TYXDXMLReader.Create();
    afIni: Result := TYXDIniReader.Create(Sender);
    afJson: Result := TYXDJsonReader.Create(Sender);
  else
    Result := nil;
  end;
end;

class function TYXDPersistent.GetWriterInterface(Sender: TObject;
  Format: TArchiveFormat): IYXDClassWriter;
begin
  case Format of
    afBinary: Result := TYXDBinaryWriter.Create(Sender);
    afXML: Result := TYXDXMLWriter.Create(Sender);
    afIni: Result := TYXDIniWriter.Create(Sender);
    afJson: Result := TYXDJsonWriter.Create(Sender);
  else
    Result := nil;
  end;
end;

procedure TYXDPersistent.AfterConstruction;
begin
  InterlockedDecrement(FRefCount);
end;

procedure TYXDPersistent.AssignObject(Src, Dest: TObject; notInherits: Boolean);
var
  PropList: TYXDPropList;
  Prop, destProp: PPropInfo;
  Kind: TTypeKind;
  i: Integer;
begin
  PropList := TYXDPropList.Create(Src);
  try
    for i := 0 to PropList.PropCount - 1 do begin
      Prop := PropList.Props[i];
      Kind := Prop.PropType^.Kind;
      if notInherits then begin
        destProp := GetPropInfo(Dest, Prop.Name);
        if (destProp = nil) or (Kind <> destProp.PropType^.Kind) then Continue;
      end;
      case Kind of
        tkUnknown:
          SetInterfaceProp(Dest, Prop, GetInterfaceProp(Src, Prop));
        tkInteger, tkChar, tkWChar, tkEnumeration, tkSet:
          SetOrdProp(Dest, Prop, GetOrdProp(Src, Prop));
        tkFloat:
          SetFloatProp(Dest, Prop, GetFloatProp(Src, Prop));
        tkString, tkLString:
          SetStrProp(Dest, Prop, GetStrProp(Src, Prop));
        tkClass:
          SetObjectProp(Dest, Prop, GetObjectProp(Src, Prop));
        tkMethod: ;
        tkWString:
          SetWideStrProp(Dest, Prop, GetWideStrProp(Src, Prop));
        tkVariant:
          SetVariantProp(Dest, Prop, GetVariantProp(Src, Prop));
        tkArray: ;
        tkRecord: ;
        tkInterface:
          SetInterfaceProp(Dest, Prop, GetInterfaceProp(Src, Prop));
        tkInt64:
          SetInt64Prop(Dest, Prop, GetInt64Prop(Src, Prop));
        tkDynArray: ;
      end;
    end;
  finally
    PropList.Free;
  end;
  if (not notInherits) and (Dest is TYXDPersistent) then
    TYXDPersistent(Dest).AssignProperties(Src);
end;

procedure TYXDPersistent.AssignProperties(Src: TObject);
begin  
end;

procedure TYXDPersistent.AssignTo(Dest: TPersistent);
begin
  if Self.InheritsFrom(Dest.ClassType) or Dest.InheritsFrom(Self.ClassType) then
    AssignObject(Self, Dest)
  else AssignObject(Self, Dest, True);
end;

procedure TYXDPersistent.BeforeDestruction;
begin
  if RefCount <> 0 then Error(reInvalidPtr);
end;

function TYXDPersistent.DefaultDateTimeFormat(tinfo: PTypeInfo): string;
begin
  if tinfo = TypeInfo(TDateTime) then Result := 'yyyy-mm-dd hh:nn:ss'
  else if tinfo = TypeInfo(TDate) then Result := 'yyyy-mm-dd'
  else if tinfo = TypeInfo(TTime) then Result := 'hh:nn:ss'
  else Result := '';
end;

function TYXDPersistent.DefineDateTimeFormat(Prop: PPropInfo): string;
begin
  Result := DefaultDateTimeFormat(Prop.PropType^);
end;

procedure TYXDPersistent.Error(errorCode: TRuntimeError);
begin
  System.Error(errorCode);
end;

function TYXDPersistent.GetClassInstance: TObject;
begin
  Result := Self;
end;

function TYXDPersistent.GetExtProp(const Name: ShortString): Variant;
begin
  Result := Null;
end;

function TYXDPersistent.GetPropAsString(Instance: TObject;
  Prop: PPropInfo): string;
var
  flt: Extended;
begin
  case Prop.PropType^.Kind of
    tkString, tkLString: Result := GetStrProp(Instance, Prop);
    tkWString: Result := GetWideStrProp(Instance, Prop);
    tkInteger: Result := IntToStr(GetOrdProp(Instance, Prop));
    tkInt64: Result := IntToStr(GetInt64Prop(Instance, Prop));
    tkEnumeration: Result := GetEnumProp(Instance, Prop);
    tkChar: Result := Chr(GetOrdProp(Instance, Prop));
    tkWChar: Result := WideChar(Word(GetOrdProp(Instance, Prop)));
    tkFloat:
      begin
        flt := GetFloatProp(Instance, Prop);
        if (Prop.PropType^ = TypeInfo(TDate)) or
          (Prop.PropType^ = TypeInfo(TTime)) or
          (Prop.PropType^ = TypeInfo(TDateTime)) then
        begin
          if flt = 0 then Result := ''
          else begin
            if Instance is TYXDPersistent then
            with TYXDPersistent(Instance) do
              Result := FormatDateTime(DefineDateTimeFormat(Prop), flt)
            else
              Result := FormatDateTime(DefaultDateTimeFormat(Prop.PropType^), flt);
          end
        end
        else Result := FloatToStr(flt);
      end;
    tkVariant: Result := GetVariantProp(Instance, Prop);
    else Result := Prop.Name;
  end;
end;

function TYXDPersistent.GetPropAsString(Instance: TObject;
  const PropName: string): string;
var
  Prop: PPropInfo;
begin
  Result := '';
  if PropName = '' then Exit;
  Prop := GetPropInfo(Instance, PropName);
  if Prop = nil then begin
    if Instance is TYXDPersistent then
      Result := VarToStr(TYXDPersistent(Instance).GetExtProp(PropName));
  end else Result := GetPropAsString(Instance, Prop);
end;

function TYXDPersistent.GetPropAsString(const Name: ShortString): string;
begin
  GetPropAsString(Self, Name);
end;

procedure TYXDPersistent.LoadFromFile(const FileName: string;
  Format: TArchiveFormat);
var
  fs: TFileStream;
begin
  if not (FileExists(FileName)) then Exit;
  fs := TFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
  try
    Self.LoadFromStream(fs, Format);
  finally
    fs.Free;
  end;
end;

procedure TYXDPersistent.LoadFromStream(avIn: TStream; Format: TArchiveFormat);
var
  ClsName: string;
  Reader: IYXDClassReader;
begin
  Reader := GetReaderInterface(Self, Format);
  if Assigned(Reader) then begin
    try
      Reader.LoadFromStream(avIn);
      Reader.BeginReadRoot(ClsName);
      LoadObject(Self, Reader);
    finally
      Reader._Release;
    end;
  end;
end;

procedure TYXDPersistent.LoadObject(Instance: TObject;
  const Reader: IYXDClassReader);
var
  PropList: TYXDPropList;
  Prop: PPropInfo;
  Kind: TTypeKind;
  i64: Int64;
  Obj: TObject;
  floatProp: Extended;
  ansiStrProp: AnsiString;
  wideStrProp: WideString;
  strProp: string;
  ansiCharProp: AnsiChar;
  wideCharProp: WideChar;
  i, j, count: Integer;
  v: Variant;
  hasMore: Boolean;
begin
  PropList := TYXDPropList.Create(Instance);
  try
    for i := 0 to PropList.PropCount - 1 do begin
      Prop := PropList.Props[i];
      Kind := Prop.PropType^.Kind;
      case Kind of
        tkUnknown: ;
        tkInteger:
          begin
            if Reader.ReadInteger(Prop.Name, j) then
              SetOrdProp(Instance, Prop, j)
            else if Prop.Default <> PROP_NO_DEF_VALUE then
              SetOrdProp(Instance, Prop, Prop.Default);
          end;
        tkChar:
          begin
            if Reader.ReadAnsiChar(Prop.Name, ansiCharProp) then
              SetOrdProp(Instance, Prop, Ord(ansiCharProp))
            else if Prop.Default <> PROP_NO_DEF_VALUE then
              SetOrdProp(Instance, Prop, Prop.Default);
          end;
        tkEnumeration:
          begin
            try
              if Reader.ReadString(Prop.Name, strProp) then
                SetEnumProp(Instance, Prop, strProp)
              else if Prop.Default <> PROP_NO_DEF_VALUE then
                SetOrdProp(Instance, Prop, Prop.Default);
            except end;
          end;
        tkFloat:
          begin
            if (Prop.PropType^ = TypeInfo(TDate)) or (Prop.PropType^ = TypeInfo(TTime))
              or (Prop.PropType^ = TypeInfo(TDateTime)) then
            begin
              if not Reader.ReadString(Prop.Name, strProp) then Continue;
              if Prop.PropType^ = TypeInfo(TDate) then
                SetFloatProp(Instance, Prop, StrToDate(strProp))
              else if Prop.PropType^ = TypeInfo(TTime) then
                SetFloatProp(Instance, Prop, StrToTime(strProp))
              else SetFloatProp(Instance, Prop, StrToDateTime(strProp));
              Continue;
            end;
            if Reader.ReadFloat(Prop.Name, floatProp) then
              SetFloatProp(Instance, Prop, floatProp);
          end;
        tkString:
          if Reader.ReadAnsiStr(Prop.Name, ansiStrProp) then begin
            SetStrProp(Instance, Prop, ansiStrProp);
          end;
        tkSet:
          begin
            if Reader.ReadString(Prop.Name, strProp) then
              SetSetProp(Instance, Prop, strProp)
            else if Prop.Default <> PROP_NO_DEF_VALUE then
              SetOrdProp(Instance, Prop, Prop.Default);
          end;
        tkClass:
          begin
            Obj := GetObjectProp(Instance, Prop);
            if Obj <> nil then ReadObject(Obj, Prop.Name, Reader);
          end;
        tkMethod: ;
        tkWChar:
          if Reader.ReadWideChar(Prop.Name, wideCharProp) then
            SetOrdProp(Instance, Prop, Integer(wideCharProp))
          else if Prop.Default <> PROP_NO_DEF_VALUE then
            SetOrdProp(Instance, Prop, Prop.Default);
        tkLString:
          if Reader.ReadAnsiStr(Prop.Name, ansiStrProp) then
            SetStrProp(Instance, Prop, ansiStrProp);
        tkWString:
          if Reader.ReadWideStr(Prop.Name, wideStrProp) then
            SetWideStrProp(Instance, Prop, wideStrProp);
        tkVariant:
          begin
            SetWideStrProp(Instance, Prop, ReadVariant(Prop.Name, Reader));
          end;
        tkArray, tkRecord, tkInterface: ;
        tkInt64:
          if Reader.ReadInt64(Prop.Name, i64) then
            SetInt64Prop(Instance, Prop, i64)
          else if Prop.Default <> PROP_NO_DEF_VALUE then
            SetInt64Prop(Instance, Prop, Prop.Default);
        tkDynArray:
          begin
            v := ReadVariant(Prop.Name, Reader);
            if (not VarIsNull(v)) then
              SetDynArrayProp(Instance, Prop, @TBytes(v)[0]);
          end;
      end;
    end;
    if Instance is TStream then begin
      Reader.ReadInteger('Size', j);
      if j > 0 then begin
        SetLength(strProp, j);
        if Reader.ReadPointer('Data', @strProp[1], j) then begin
          if TStream(Instance).Size <> Length(strProp) then        
            TStream(Instance).Size := Length(strProp);
          i := TStream(Instance).Position;
          TStream(Instance).Position := 0;
          TStream(Instance).WriteBuffer(strProp[1], Length(strProp));
          TStream(Instance).Position := i;
        end;
        strProp := '';
      end;
    end else if Instance is TStrings then begin
      TStrings(Instance).Clear;
      j := 0;
      if Reader.BeginReadList(Count) then begin
        while Reader.ReadStrListItem(j, strProp, hasMore) do begin
          Inc(j);
          TStrings(Instance).Add(strProp);
          if not hasMore then Break;
        end;
        Reader.EndReadList;
      end;
    end else if Instance is TCollection then begin
      TCollection(Instance).Clear;
      if Reader.BeginReadList(Count) then begin
        j := 0;
        while Reader.BeginReadListItem(j) do begin
          Inc(j);
          LoadObject(TCollection(Instance).Add, Reader);
          if not Reader.EndReadListItem then Break;
        end;
        Reader.EndReadList;
      end;
    end;
  finally
    PropList.Free;
  end;
  if Instance is TYXDPersistent then
    TYXDPersistent(Instance).ReadProperties(Reader);
end;

class function TYXDPersistent.NewInstance: TObject;
begin
  Result := inherited NewInstance;
  TYXDPersistent(Result).FRefCount := 1;
end;

function TYXDPersistent.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  if GetInterface(IID, Obj) then
    Result := S_OK
  else
    Result := E_NOINTERFACE;   
end;

procedure TYXDPersistent.ReadObject(Instance: TObject; const Name: string;
  const Reader: IYXDClassReader);
begin
  if Reader.BeginRead(Name) then begin
    LoadObject(Instance, Reader);
    Reader.EndRead;
  end;
end;

procedure TYXDPersistent.ReadProperties(const Reader: IYXDClassReader);
begin
end;

function TYXDPersistent.ReadVariant(const Name: string;
  const Reader: IYXDClassReader): Variant;
var
  I64: Int64;
  IFolat: Extended;
  t: Integer;
  p: Pointer;
  iStr: string;
begin
  try
    Reader.BeginRead(Name);
    try
      Reader.ReadInteger('Type', t);
      if t = varInt64 then begin
        Reader.Readint64('Value', I64);
        Result := I64;
      end else if t = varInteger then begin
        Reader.ReadInteger('Value', t);
        Result := t;
      end else if t = varCurrency then begin
        Reader.ReadFloat('Value', IFolat);
        Result := IFolat;
      end else if t = varString then begin
        Reader.ReadString('Value', iStr);
        Result := iStr;
      end else if t = varArray then begin
        Result := Null;
        Reader.ReadInteger('Length', t);
        if (t <= 0) then Exit;
        Result := VarArrayCreate([0, t - 1], varByte);
        p := VarArrayLock(Result);
        try
          Reader.ReadPointer('Value', p, t);
        finally
          VarArrayUnlock(Result);
        end;
      end else
        Result := Null;
    finally
      Reader.EndRead;
    end;
  except
    Result := Null;
  end;
end;

procedure TYXDPersistent.SaveObject(Instance: TObject;
  const Writer: IYXDClassWriter);
var
  PropList: TYXDPropList;
  Prop: PPropInfo;
  Kind: TTypeKind;
  i64: Int64;
  Obj: TObject;
  i, j: Integer;
  Buf: TBytes;
begin
  PropList := TYXDPropList.Create(Instance);
  try
    for i := 0 to PropList.PropCount - 1 do begin
      Prop := PropList.Props[i];
      Kind := Prop.PropType^.Kind;
      case Kind of
        tkUnknown: ;
        tkInteger:
          begin
            j := GetOrdProp(Instance, Prop);
            Writer.WriteInteger(Prop.Name, j);
          end;
        tkChar:
          begin
            j := GetOrdProp(Instance, Prop);
            Writer.WriteAnsiChar(Prop.Name, AnsiChar(j));
          end;
        tkEnumeration:
          begin
            Writer.WriteString(Prop.Name, GetEnumProp(Instance, Prop));
          end;
        tkFloat:
          begin
            if Prop.PropType^ = TypeInfo(TDate) then
              Writer.WriteString(Prop.Name, DateToStr(GetFloatProp(Instance, Prop)))
            else if Prop.PropType^ = TypeInfo(TTime) then
              Writer.WriteString(Prop.Name, TimeToStr(GetFloatProp(Instance, Prop)))
            else if Prop.PropType^ = TypeInfo(TDateTime) then
              Writer.WriteString(Prop.Name, DateTimeToStr(GetFloatProp(Instance, Prop)))
            else
              Writer.WriteFloat(Prop.Name, GetFloatProp(Instance, Prop));
          end;
        tkString, tkLString:
          begin
            Writer.WriteAnsiStr(Prop.Name, GetStrProp(Instance, Prop));
          end;
        tkSet:
          begin
            Writer.WriteString(Prop.Name, GetSetProp(Instance, Prop));
          end;
        tkClass:
          begin
            Obj := GetObjectProp(Instance, Prop);
            if Obj <> nil then WriteObject(Obj, Prop.Name, Writer);
          end;
        tkWChar:
          begin
            j := GetOrdProp(Instance, Prop);
            Writer.WriteWideChar(Prop.Name, WideChar(j));
          end;
        tkWString:
          begin
            Writer.WriteWideStr(Prop.Name, GetWideStrProp(Instance, Prop));
          end;
        tkVariant:
          begin
            WriteVariant(GetVariantProp(Instance, Prop), Prop.Name, nil, Writer);
          end;
        tkArray, tkRecord, tkInterface: ;
        tkInt64:
          begin
            i64 := GetInt64Prop(Instance, Prop);
            Writer.WriteInt64(Prop.Name, i64);
          end;
        tkDynArray:
          begin
            WriteVariant(GetPropValue(Instance, Prop), Prop.Name, GetTypeData(Prop.PropType^), Writer);
          end;
      end;
    end;
    if Instance is TStream then begin
      j := TStream(Instance).Size;
      Writer.WriteInteger('Size', j);
      if j > 0 then begin
        SetLength(Buf, j);
        i := TStream(Instance).Position;
        TStream(Instance).Position := 0;
        TStream(Instance).ReadBuffer(Buf[0], j);
        if High(Buf) >= 0 then 
          Writer.WritePointer('Data', @Buf[0], j);
        TStream(Instance).Position := i;
      end;
    end else if Instance is TStrings then begin
      if Writer.BeginWriteList(tkString, TStrings(Instance).Count) then begin
        for i := 0 to TStrings(Instance).Count - 1 do
          Writer.WriteStrListItem(i, TStrings(Instance)[i]);
        Writer.EndWriteList;
      end;
    end else if Instance is TCollection then begin
      if Writer.BeginWriteList(tkClass, TCollection(Instance).Count) then begin
        for i := 0 to TCollection(Instance).Count - 1 do begin
          Obj := TCollection(Instance).Items[i];
          Writer.BeginWriteListItem(i);
          SaveObject(Obj, Writer);
          Writer.EndWriteListItem;
        end;
        Writer.EndWriteList;
      end;
    end;
  finally
    PropList.Free;
  end;
  if Instance is TYXDPersistent then
    TYXDPersistent(Instance).WriteProperties(Writer);
end;

procedure TYXDPersistent.SaveToFile(const FileName: string;
  Format: TArchiveFormat);
var
  fs: TFileStream;
begin
  if not DirectoryExists(ExtractFilePath(FileName)) then
    CreateDir(ExtractFilePath(FileName));
  fs := TFileStream.Create(FileName, fmCreate or fmOpenWrite);
  try
    Self.SaveToStream(fs, Format);
  finally
    fs.Free;
  end;
end;

procedure TYXDPersistent.SaveToStream(avOut: TStream; Format: TArchiveFormat);
var
  Writer: IYXDClassWriter;
begin
  Writer := GetWriterInterface(Self, Format);
  if Assigned(Writer) then begin
    try
      Writer.BeginWriteRoot(Self.ClassName);
      SaveObject(Self, Writer);
      Writer.EndWriteRoot;
      Writer.SaveToStream(avOut);
    finally
      Writer._Release;
    end;
  end;
end;

procedure TYXDPersistent.SetPropAsString(const Name: ShortString;
  const value: string);
begin
  SetPropAsString(Self, Name, value); 
end;

procedure TYXDPersistent.SetPropAsString(Instance: TObject; const PropName,
  value: string);
var
  Prop: PPropInfo;
begin
  if PropName = '' then Exit;
  Prop := GetPropInfo(Instance, PropName);
  if Prop = nil then begin
    if Instance is TYXDPersistent then
      TYXDPersistent(Instance).SetExtProp(PropName, value);
  end else SetPropAsString(Instance, Prop, value);
end;

procedure TYXDPersistent.SetExtProp(const Name: ShortString;
  const value: Variant);
begin
end;

procedure TYXDPersistent.SetPropAsString(Instance: TObject; Prop: PPropInfo;
  const value: string);
var
  c, v: Integer;
  v64: Int64;
begin
  case Prop.PropType^.Kind of
    tkString, tkLString: SetStrProp(Instance, Prop, value);
    tkWString: SetWideStrProp(Instance, Prop, value);
    tkInteger:
      begin
        Val(value, v, c);
        if c = 0 then SetOrdProp(Instance, Prop, v);
      end;
    tkInt64:
      begin
        Val(value, v64, c);
        if c = 0 then SetInt64Prop(Instance, Prop, v64);
      end;
    tkEnumeration:
      begin
        Val(value, v, c);
        if c = 0 then SetOrdProp(Instance, Prop, v)
        else SetEnumProp(Instance, Prop, value);
      end;
    tkChar: SetOrdProp(Instance, Prop, Ord(value[1]));
    tkWChar: SetOrdProp(Instance, Prop, Ord(WideString(value)[1]));
    tkFloat:
      begin
        if (Prop.PropType^ = TypeInfo(TDate)) then
          SetFloatProp(Instance, Prop, StrToDate(value))
        else if (Prop.PropType^ = TypeInfo(TTime)) then
          SetFloatProp(Instance, Prop, StrToTime(value))
        else if (Prop.PropType^ = TypeInfo(TDateTime)) then
          SetFloatProp(Instance, Prop, StrToDateTime(value))
        else SetFloatProp(Instance, Prop, StrToFloatDef(value, 0));
      end;
    tkVariant: SetVariantProp(Instance, Prop, value);
  end;
end;

procedure TYXDPersistent.WriteObject(Instance: TObject; const Name: string;
  const Writer: IYXDClassWriter);
begin
  Writer.BeginWrite(Name);
  SaveObject(Instance, Writer);
  Writer.EndWrite;
end;

procedure TYXDPersistent.WriteProperties(const Writer: IYXDClassWriter);
begin
end;  

procedure TYXDPersistent.WriteVariant(const Value: Variant; const Name: string;
  TypeData: PTypeData; const Writer: IYXDClassWriter);
var
  i: Integer;
  p: Pointer;
begin
  try
    if FindVarData(Value)^.VType = varInt64 then begin
      Writer.BeginWrite(Name);
      Writer.WriteInteger('Type', varInt64);
      Writer.WriteInt64('Value', Value);
      Writer.EndWrite;
    end else if VarIsOrdinal(Value) then begin
      Writer.BeginWrite(Name);
      Writer.WriteInteger('Type', varInteger);
      Writer.WriteInteger('Value', Value);
      Writer.EndWrite;
    end else if VarIsFloat(Value) then begin
      Writer.BeginWrite(Name);
      Writer.WriteInteger('Type', varCurrency);
      Writer.WriteFloat('Value', Value);
      Writer.EndWrite;
    end else if VarIsStr(Value) then begin
      Writer.BeginWrite(Name);
      Writer.WriteInteger('Type', varString);
      Writer.WriteString('Value', Value);
      Writer.EndWrite;
    end else if (TypeData <> nil) and VarIsArray(Value) then begin
      if TypeData.varType <> varByte then Exit; // 只处理字节数组
      Writer.BeginWrite(Name);
      Writer.WriteInteger('Type', varArray);
      i := VarArrayHighBound(Value, 1) + 1;
      Writer.WriteInteger('Length', i);
      if i > 0 then begin
        p := VarArrayLock(Value);
        try
          Writer.WritePointer('Value', p, i);
        finally
          VarArrayUnlock(Value);
        end;
      end;
      Writer.EndWrite;
    end;
  except end;
end;

function TYXDPersistent._AddRef: Integer;
begin
  Result := InterlockedIncrement(FRefCount);
end;

function TYXDPersistent._Release: Integer;
begin
  Result := InterlockedDecrement(FRefCount);
  if Result = 0 then
    Destroy;
end;

{ TYXDPersistentList }

function TYXDPersistentList.Add: TYXDPersistent;
var
  s: string;
  Item: TYXDPersistent;
begin
  Item := CreateClass;
  s := Item.ClassName;
  fObjects.Add(Pointer(Item));
  Result := Item;
end;

procedure TYXDPersistentList.AddToList(Obj: TYXDPersistent);
begin
  fObjects.Add(Pointer(Obj)); 
end;

procedure TYXDPersistentList.AssignProperties(Src: TObject);
var
  i: Integer;
begin
  inherited;
  if Src is TYXDPersistentList then begin
    Self.Clear;
    for i := 0 to TYXDPersistentList(Src).Count - 1 do
      Self.Add.Assign(TYXDPersistentList(Src).Items[i]);
  end;
end;

procedure TYXDPersistentList.Clear;
var
  i: Integer;
begin
  if fObjects = nil then Exit;
  for i := 0 to fObjects.Count - 1 do
    TObject(fObjects.Items[i]).Free;
  fObjects.Clear;
end;

constructor TYXDPersistentList.Create;
begin
  if fItemClass = nil then raise Exception.Create('Does not specify a ItemClass.');
  inherited;
  fObjects := TList.Create;
end;

constructor TYXDPersistentList.Create(ItemCls: TYXDPersistentClass);
begin
  fItemClass := ItemCls; 
  Create;
end;

function TYXDPersistentList.CreateClass: TYXDPersistent;
begin
  Result := fItemClass.Create;
end;

destructor TYXDPersistentList.Destroy;
begin
  if fObjects <> nil then begin
    Clear;
    fObjects.Free;
  end;
  inherited;
end;

procedure TYXDPersistentList.Exchange(Index1, Index2: Integer);
begin
  fObjects.Exchange(Index1, Index2);
end;

function TYXDPersistentList.Extract(Index: Integer): TYXDPersistent;
begin
  Result := TYXDPersistent(fObjects[Index]);
  fObjects.Delete(Index);
end;

function TYXDPersistentList.GetItem(Index: Integer): TYXDPersistent;
begin
  Result := TYXDPersistent(fObjects[Index]);
end;

function TYXDPersistentList.GetSize: Integer;
begin
  Result := fObjects.Count;
end;

function TYXDPersistentList.IndexOf(Item: TYXDPersistent): Integer;
begin
  Result := fObjects.IndexOf(Pointer(Item));
end;

procedure TYXDPersistentList.Insert(Index: Integer; Item: TYXDPersistent);
begin
  fObjects.Insert(Index, Item);
end;

procedure TYXDPersistentList.LoadFromDataSet(Dataset: TDataSet; num: Integer;
  Reader: IYXDClassReader);
var
  i: Integer;
begin
  if (num = -1) or (num > Dataset.RecordCount - Dataset.RecNo + 1) then
    num := Dataset.RecordCount - Dataset.RecNo + 1;
  if num = 0 then Exit;
  Self.Clear;
  if not Assigned(Reader) then
    Exit;
  for i := 0 to num - 1 do
  begin
    LoadObject(Self.Add, Reader);
    Dataset.Next;
  end;
end;

procedure TYXDPersistentList.ReadProperties(const Reader: IYXDClassReader);
var
  ItemCount, i: Integer;
  Obj: TYXDPersistent;
begin
  inherited;
  Clear;
  if Reader.BeginReadList(ItemCount) then begin
    i := 0;
    while Reader.BeginReadListItem(i) do begin
      Inc(i);
      Obj := Add;
      LoadObject(Obj, Reader);
      if not Reader.EndReadListItem then Break;
    end;
    Reader.EndReadList;
  end;
end;

procedure TYXDPersistentList.Remove(Index: Integer);
begin
  TObject(fObjects.Items[Index]).Free;
  fObjects.Delete(Index);
end;

procedure TYXDPersistentList.SetItem(Index: Integer; Item: TYXDPersistent);
var
  cur: TYXDPersistent;
begin
  cur := TYXDPersistent(fObjects[Index]);
  if cur = Item then Exit;
  cur.Free;
  fObjects.Items[Index] := Pointer(Item);
end;

procedure TYXDPersistentList.WriteProperties(const Writer: IYXDClassWriter);
var
  i: Integer;
  ItemObject: TYXDPersistent;
begin
  inherited;
  if Writer.BeginWriteList(tkClass, Self.Count) then begin
    for i := 0 to Self.Count - 1 do begin
      ItemObject := Self.Items[i];
      Writer.BeginWriteListItem(i);
      SaveObject(ItemObject, Writer);
      Writer.EndWriteListItem;
    end;
    Writer.EndWriteList;
  end;
end;

{ TYXDIniWriter }

function TYXDIniWriter.BeginWrite(const Name: string): Boolean;
begin
  Hierarchy.Add(Name);
  SectionName := SectionName + '.' + Name;
  Result := True;
end;

function TYXDIniWriter.BeginWriteList(ListKind: TTypeKind;
  ItemCount: Integer): Boolean;
begin
  Ini.WriteInteger(SectionName, 'ItemCount', ItemCount);
  Result := True;
end;

function TYXDIniWriter.BeginWriteListItem(Index: Integer): Boolean;
var
  tag: AnsiString;  
begin
  tag := Format('Item%d', [Index]);
  Hierarchy.Add(tag);
  SectionName := Format('%s.%s', [SectionName, tag]);
  Result := True;
end;

function TYXDIniWriter.BeginWriteRoot(const ClsName: string): Boolean;
begin
  Hierarchy.Clear;
  Hierarchy.Add('Root');
  SectionName := 'Root';
  Result := True;
end;

constructor TYXDIniWriter.Create(aInstance: TObject);
begin
  Hierarchy := TStringList.Create;
  Ini := TMemIniFile.Create(''); 
end;

destructor TYXDIniWriter.Destroy;
begin
  Ini.Free;
  Hierarchy.Free;
  inherited;
end;

procedure TYXDIniWriter.EndWrite;
begin
  SetLength(SectionName, Length(SectionName) - 1 -
    Length(Hierarchy[Hierarchy.Count - 1]));
  Hierarchy.Delete(Hierarchy.Count - 1);
end;

procedure TYXDIniWriter.EndWriteList;
begin
end;

procedure TYXDIniWriter.EndWriteListItem;
begin
  SetLength(SectionName, Length(SectionName) - 1 -
    Length(Hierarchy[Hierarchy.Count - 1]));
  Hierarchy.Delete(Hierarchy.Count - 1);
end;

procedure TYXDIniWriter.EndWriteRoot;
begin
  SetLength(SectionName, Length(SectionName) - 1 -
    Length(Hierarchy[Hierarchy.Count - 1]));
  Hierarchy.Delete(Hierarchy.Count - 1);
end;

procedure TYXDIniWriter.SaveToStream(avOut: TStream);
var
  S: string;
  List: TStringList;
begin
  List := TStringList.Create;
  try
    Ini.GetStrings(List);
    S := List.Text;
    avOut.Write(Pointer(S)^, Length(S) * SizeOf(Char));
  finally
    List.Free;
  end;
end;

procedure TYXDIniWriter.WriteAnsiChar(const Name: string; Value: AnsiChar);
begin
  Ini.WriteString(SectionName, Name, value);
end;

procedure TYXDIniWriter.WriteAnsiStr(const Name: string;
  const Value: AnsiString);
begin
  Ini.WriteString(SectionName, Name, value);
end;

procedure TYXDIniWriter.WriteAnsiStrListItem(Index: Integer;
  const Value: AnsiString);
begin
  Ini.WriteString(SectionName, Format('Item%d', [Index]), Value);
end;

procedure TYXDIniWriter.WriteByte(const Name: string; Value: Byte);
begin
  Ini.WriteInteger(SectionName, Name, value);
end;

procedure TYXDIniWriter.WriteChar(const Name: string; Value: Char);
begin
  Ini.WriteString(SectionName, Name, value);
end;

procedure TYXDIniWriter.WriteFloat(const Name: string; Value: Extended);
begin
  Ini.WriteFloat(SectionName, Name, Value);
end;

procedure TYXDIniWriter.WriteInt64(const Name: string; Value: Int64);
begin
  Ini.WriteString(SectionName, Name, IntToStr(Value));
end;

procedure TYXDIniWriter.WriteInteger(const Name: string; Value: Integer);
begin
  Ini.WriteInteger(SectionName, Name, Value); 
end;

procedure TYXDIniWriter.WritePointer(const Name: string; Buf: Pointer;
  Len: Integer);
var
  Buffer: string;
begin
  if (Len <= 0) then Exit;
  SetLength(Buffer, Len * 2);
  BinToHex(Buf, @Buffer[1], Len);
  Ini.WriteString(SectionName, Name, Buffer);
  SetLength(Buffer, 0);
end;

procedure TYXDIniWriter.WriteString(const Name, Value: string);
begin
  Ini.WriteString(SectionName, Name, Value);
end;

procedure TYXDIniWriter.WriteStrListItem(Index: Integer; const Value: string);
begin
  Ini.WriteString(SectionName, Format('Item%d', [Index]), Value);
end;

procedure TYXDIniWriter.WriteWideChar(const Name: string; Value: WideChar);
begin
  Ini.WriteString(SectionName, Name, Value);
end;

procedure TYXDIniWriter.WriteWideStr(const Name: string;
  const Value: WideString);
begin
  Ini.WriteString(SectionName, Name, Value);
end;

procedure TYXDIniWriter.WriteWideStrListItem(Index: Integer;
  const Value: WideString);
begin
  Ini.WriteString(SectionName, Format('Item%d', [Index]), Value);
end;

procedure TYXDIniWriter.WriteWord(const Name: string; Value: Word);
begin
  Ini.WriteInteger(SectionName, Name, Value);
end;

{ TYXDIniReader }

function TYXDIniReader.BeginRead(const Name: string): Boolean;
begin
  Hierarchy.Add(Name);
  SectionName := SectionName + '.' + Name;
  Result := True;
end;

function TYXDIniReader.BeginReadList(out Count: Integer): Boolean;
begin
  Count := Ini.ReadInteger(SectionName, 'ItemCount', 0);
  Result := True;
end;

function TYXDIniReader.BeginReadListItem(Index: Integer): Boolean;
var
  tag, sec: string;
begin
  tag := Format('Item%d', [Index]);
  sec := SectionName + '.' + tag;
  Result := Ini.SectionExists(sec);
  if Result then
  begin
    Hierarchy.Add(tag);
    SectionName := sec;
    Result := True;
  end;
end;

function TYXDIniReader.BeginReadRoot(out ClsName: string): Boolean;
begin
  ClsName := 'Root';
  Hierarchy.Clear;
  Hierarchy.Add(ClsName);
  SectionName := ClsName;
  Result := True;  
end;

constructor TYXDIniReader.Create(aInstance: TObject);
begin
  Hierarchy := TStringList.Create;
  Ini := TMemIniFile.Create(''); 
end;

destructor TYXDIniReader.Destroy;
begin
  Ini.Free;
  Hierarchy.Free;
  inherited;
end;

procedure TYXDIniReader.EndRead;
begin
  SetLength(SectionName, Length(SectionName) - 1 -
    Length(Hierarchy[Hierarchy.Count - 1]));
  Hierarchy.Delete(Hierarchy.Count - 1);
end;

procedure TYXDIniReader.EndReadList;
begin 
end;

function TYXDIniReader.EndReadListItem: Boolean;
begin
  SetLength(SectionName, Length(SectionName) - 1 -
    Length(Hierarchy[Hierarchy.Count - 1]));
  Hierarchy.Delete(Hierarchy.Count - 1);
  Result := True;
end;

function TYXDIniReader.IniReadString(const ident: string;
  out value: string): Boolean;
begin
  Result := Ini.ValueExists(SectionName, ident);
  if Result then value := Ini.ReadString(SectionName, ident, '');
end;

procedure TYXDIniReader.LoadFromStream(avIn: TStream);
var
  Strs: TStringList;
  S: AnsiString;
begin
  SetLength(S, avIn.Size - avIn.Position);
  avIn.Read(Pointer(S)^, Length(s));
  Strs := TStringList.Create;
  try
    Strs.Text := S;
    Ini.SetStrings(Strs);
  finally
    Strs.Free;
  end;
end;

function TYXDIniReader.ReadAnsiChar(const Name: string;
  out Value: AnsiChar): Boolean;
var
  str: AnsiString;
begin
  Result := ReadAnsiStr(Name, str) and (str <> '');
  if Result then value := str[1];
end;

function TYXDIniReader.ReadAnsiStr(const Name: string;
  out Value: AnsiString): Boolean;
begin
  Result := Ini.ValueExists(SectionName, Name);
  if Result then value := Ini.ReadString(SectionName, Name, '');
end;

function TYXDIniReader.ReadAnsiStrListItem(Index: Integer;
  out Value: AnsiString; out More: Boolean): Boolean;
begin
  Result := IniReadString(Format('Item%d', [Index]), value);
  more := True;
end;

function TYXDIniReader.ReadByte(const Name: string; out Value: Byte): Boolean;
var
  i32: Integer;
begin
  Result := ReadInteger(Name, i32);
  Value := i32;
end;

function TYXDIniReader.ReadChar(const Name: string; out Value: Char): Boolean;
var
  str: string;
begin
  Result := IniReadString(Name, str);
  if Result then value := str[1];
end;

function TYXDIniReader.ReadFloat(const Name: string;
  out Value: Extended): Boolean;
var
  str: string;
begin
  str := Ini.ReadString(SectionName, Name, '');
  try
    value := StrToFloat(str);
    Result := True;
  except
    Result := False;
  end;
end;

function TYXDIniReader.ReadInt64(const Name: string; out Value: Int64): Boolean;
var
  c: Integer;
  str: string;
begin
  str := Ini.ReadString(SectionName, Name, '');
  Val(str, value, c);
  Result := c = 0;
end;

function TYXDIniReader.ReadInteger(const Name: string;
  out Value: Integer): Boolean;
var
  c: Integer;
  str: string;
begin
  str := Ini.ReadString(SectionName, Name, '');
  Val(str, value, c);
  Result := c = 0;
end;

function TYXDIniReader.ReadPointer(const Name: string; Buf: Pointer;
  var Len: Integer): Boolean;
var
  Buffer: string;
begin
  Result := False;
  if (Len <= 0) then Exit;
  Buffer := Ini.ReadString(SectionName, Name, '');
  if Length(Buffer) <> Len * 2 then Exit;
  HexToBin(@Buffer[1], Buf, Len);
  Result := True;
end;

function TYXDIniReader.ReadString(const Name: string;
  out Value: string): Boolean;
begin
  Result := IniReadString(Name, Value);
end;

function TYXDIniReader.ReadStrListItem(Index: Integer; out Value: string;
  out More: Boolean): Boolean;
begin
  Result := IniReadString(Format('Item%d', [Index]), value);
  more := True;
end;

function TYXDIniReader.ReadWideChar(const Name: string;
  out Value: WideChar): Boolean;
var
  wstr: WideString;
begin
  Result := ReadWideStr(Name, wstr) and (wstr <> '');
  if Result then value := wstr[1];
end;

function TYXDIniReader.ReadWideStr(const Name: string;
  out Value: WideString): Boolean;
begin
  Result := Ini.ValueExists(SectionName, Name);
  if Result then value := Ini.ReadString(SectionName, Name, '');
end;

function TYXDIniReader.ReadWideStrListItem(Index: Integer;
  out Value: WideString; out More: Boolean): Boolean;
var
  s: string;
begin
  Result := IniReadString(Format('Item%d', [Index]), s);
  if Result then value := WideString(s);
  more := True;
end;

function TYXDIniReader.ReadWord(const Name: string; out Value: Word): Boolean;
var
  i32: Integer;
begin
  Result := ReadInteger(Name, i32);
  Value := i32;
end;

{ TYXDBinaryWriter }

function TYXDBinaryWriter.BeginWrite(const Name: string): Boolean;
begin
  Result := True;    
end;

function TYXDBinaryWriter.BeginWriteList(ListKind: TTypeKind;
  ItemCount: Integer): Boolean;
begin
  WriteInteger(Mem, ItemCount);
  Result := True;
end;

function TYXDBinaryWriter.BeginWriteListItem(Index: Integer): Boolean;
begin
  Result := True;
end;

function TYXDBinaryWriter.BeginWriteRoot(const ClsName: string): Boolean;
begin
  Mem.Clear;
  Result := True;
end;

constructor TYXDBinaryWriter.Create(aInstance: TObject);
begin
  Mem := TMemoryStream.Create(); 
end;

destructor TYXDBinaryWriter.Destroy;
begin
  Mem.Free;
  inherited;
end;

procedure TYXDBinaryWriter.EndWrite;
begin
end;

procedure TYXDBinaryWriter.EndWriteList;
begin
end;

procedure TYXDBinaryWriter.EndWriteListItem;
begin
end;

procedure TYXDBinaryWriter.EndWriteRoot;
begin
end;

procedure TYXDBinaryWriter.SaveToStream(avOut: TStream);
begin
  avOut.CopyFrom(Mem, 0);
end;

procedure TYXDBinaryWriter.WriteAnsiChar(const Name: string; Value: AnsiChar);
begin
  Mem.Write(Value, SizeOf(Value));
end;

procedure TYXDBinaryWriter.WriteAnsiStr(const Name: string;
  const Value: AnsiString);
var
  l: Word;
begin
  l := Length(Value);
  Self.WriteWord(Mem, l);
  if l > 0 then
    Mem.WriteBuffer(Value[1], l);
end;

procedure TYXDBinaryWriter.WriteAnsiStrListItem(Index: Integer;
  const Value: AnsiString);
var
  l: Word;
begin
  l := Length(Value);
  Self.WriteWord(Mem, l);
  if l > 0 then
    Mem.WriteBuffer(Value[1], l);
end;

procedure TYXDBinaryWriter.WriteByte(const Name: string; Value: Byte);
begin
  Mem.WriteBuffer(Value, SizeOf(Value));
end;

procedure TYXDBinaryWriter.WriteChar(const Name: string; Value: Char);
begin
  Mem.WriteBuffer(Value, SizeOf(Value));
end;

procedure TYXDBinaryWriter.WriteFloat(const Name: string; Value: Extended);
begin
  Mem.WriteBuffer(Value, SizeOf(Value));
end;

procedure TYXDBinaryWriter.WriteInt64(const Name: string; Value: Int64);
begin
  Mem.WriteBuffer(Value, SizeOf(Value));
end;

procedure TYXDBinaryWriter.WriteInteger(avOut: TStream; avData: Integer);
begin
  avOut.WriteBuffer(avData, SizeOf(avData));
end;

procedure TYXDBinaryWriter.WriteInteger(const Name: string; Value: Integer);
begin
  WriteInteger(Mem, Value);
end;

procedure TYXDBinaryWriter.WritePointer(const Name: string; Buf: Pointer;
  Len: Integer);
begin
  Mem.Write(Buf^, Len);
end;

procedure TYXDBinaryWriter.WriteString(const Name, Value: string);
var
  l: Word;
begin
  l := Length(Value);
  Self.WriteWord(Mem, l);
  if l > 0 then
    Mem.WriteBuffer(Value[1], l);
end;

procedure TYXDBinaryWriter.WriteStrListItem(Index: Integer;
  const Value: string);
var
  l: Word;
begin
  l := Length(Value);
  Self.WriteWord(Mem, l);
  if l > 0 then
    Mem.WriteBuffer(Value[1], l);
end;

procedure TYXDBinaryWriter.WriteWideChar(const Name: string; Value: WideChar);
begin
  Mem.WriteBuffer(Value, SizeOf(Value));
end;

procedure TYXDBinaryWriter.WriteWideStr(const Name: string;
  const Value: WideString);
var
  l: Word;
begin
  l := Length(Value);
  Self.WriteWord(Mem, l);
  if l > 0 then
    Mem.WriteBuffer(Value[1], l);
end;

procedure TYXDBinaryWriter.WriteWideStrListItem(Index: Integer;
  const Value: WideString);
var
  l: Word;
begin
  l := Length(Value);
  Self.WriteWord(Mem, l);
  if l > 0 then
    Mem.WriteBuffer(Value[1], l);
end;

procedure TYXDBinaryWriter.WriteWord(avOut: TStream; avData: Word);
begin
  avOut.WriteBuffer(avData, SizeOf(avData));
end;

procedure TYXDBinaryWriter.WriteWord(const Name: string; Value: Word);
begin
  Mem.WriteBuffer(Value, SizeOf(Value));
end;

{ TYXDBinaryReader }

function TYXDBinaryReader.BeginRead(const Name: string): Boolean;
begin
  Result := True;
end;

function TYXDBinaryReader.BeginReadList(out Count: Integer): Boolean;
begin
  Count := ReadInteger(Mem);
  Result := True;
end;

function TYXDBinaryReader.BeginReadListItem(Index: Integer): Boolean;
begin
  Result := True;
end;

function TYXDBinaryReader.BeginReadRoot(out ClsName: string): Boolean;
begin
  Result := True;
  Mem.Position := 0;
end;

constructor TYXDBinaryReader.Create(aInstance: TObject);
begin
  Mem := TMemoryStream.Create;
end;

destructor TYXDBinaryReader.Destroy;
begin
  Mem.Free;
  inherited;
end;

procedure TYXDBinaryReader.EndRead;
begin
end;

procedure TYXDBinaryReader.EndReadList;
begin
end;

function TYXDBinaryReader.EndReadListItem: Boolean;
begin
  Result := True;
end;

procedure TYXDBinaryReader.LoadFromStream(avIn: TStream);
begin
  Mem.Clear;
  Mem.CopyFrom(avIn, 0);
end;

function TYXDBinaryReader.ReadAnsiChar(const Name: string;
  out Value: AnsiChar): Boolean;
begin
  Mem.Read(Value, SizeOf(Value));
  Result := True;
end;

function TYXDBinaryReader.ReadAnsiStr(const Name: string;
  out Value: AnsiString): Boolean;
var
  l: Integer;
begin
  l := Self.ReadWord(Mem);
  SetLength(Value, l);
  if l > 0 then
    Mem.ReadBuffer(Value[1], l);
  Result := True;
end;

function TYXDBinaryReader.ReadAnsiStrListItem(Index: Integer;
  out Value: AnsiString; out More: Boolean): Boolean;
var
  l: Integer;
begin
  l := Self.ReadWord(Mem);
  SetLength(Value, l);
  if l > 0 then
    Mem.ReadBuffer(Value[1], l);
  more := True;
  Result := True;
end;

function TYXDBinaryReader.ReadByte(const Name: string;
  out Value: Byte): Boolean;
begin
  Mem.Read(Value, SizeOf(Value));
  Result := True;
end;

function TYXDBinaryReader.ReadChar(const Name: string;
  out Value: Char): Boolean;
begin
  Mem.Read(Value, SizeOf(Value));
  Result := True;
end;

function TYXDBinaryReader.ReadFloat(const Name: string;
  out Value: Extended): Boolean;
begin
  Mem.Read(Value, SizeOf(Value));
  Result := True;
end;

function TYXDBinaryReader.ReadInt64(const Name: string;
  out Value: Int64): Boolean;
begin
  Mem.Read(Value, SizeOf(Value));
  Result := True;
end;

function TYXDBinaryReader.ReadInteger(avIn: TStream): Integer;
begin
  avIn.ReadBuffer(Result, SizeOf(Result));
end;

function TYXDBinaryReader.ReadInteger(const Name: string;
  out Value: Integer): Boolean;
begin
  Value := ReadInteger(Mem);
  Result := True;
end;

function TYXDBinaryReader.ReadPointer(const Name: string; Buf: Pointer;
  var Len: Integer): Boolean;
begin
  Mem.ReadBuffer(Buf^, Len);
  Result := True;
end;

function TYXDBinaryReader.ReadString(const Name: string;
  out Value: string): Boolean;
var
  l: Integer;
begin
  l := Self.ReadWord(Mem);
  SetLength(Value, l);
  if l > 0 then
    Mem.ReadBuffer(Value[1], l);
  Result := True;
end;

function TYXDBinaryReader.ReadStrListItem(Index: Integer; out Value: string;
  out More: Boolean): Boolean;
begin
  Mem.Read(Value, SizeOf(Value));
  Result := True;
end;

function TYXDBinaryReader.ReadWideChar(const Name: string;
  out Value: WideChar): Boolean;
begin
  Mem.Read(Value, SizeOf(Value));
  Result := True;
end;

function TYXDBinaryReader.ReadWideStr(const Name: string;
  out Value: WideString): Boolean;
var
  l: Integer;
begin
  l := Self.ReadWord(Mem);
  SetLength(Value, l);
  if l > 0 then
    Mem.ReadBuffer(Value[1], l);
  Result := True;
end;

function TYXDBinaryReader.ReadWideStrListItem(Index: Integer;
  out Value: WideString; out More: Boolean): Boolean;
var
  l: Integer;
begin
  l := Self.ReadWord(Mem);
  SetLength(Value, l);
  if l > 0 then
    Mem.ReadBuffer(Value[1], l);
  more := True;
  Result := True;
end;

function TYXDBinaryReader.ReadWord(const Name: string;
  out Value: Word): Boolean;
begin
  Mem.Read(Value, SizeOf(Value));
  Result := True;
end;

function TYXDBinaryReader.ReadWord(avIn: TStream): Word;
begin
  avIn.ReadBuffer(Result, SizeOf(Result));
end;

{ TYXDXMLWriter }

function TYXDXMLWriter.BeginWrite(const Name: string): Boolean;
begin
  fRootObject := fRootObject.AddChild(Name);
  Result := True;
end;

function TYXDXMLWriter.BeginWriteList(ListKind: TTypeKind;
  ItemCount: Integer): Boolean;
begin
  fRootObject := fRootObject.AddChild(CSTR_COLLECTION_TAG);
  Result := True;
end;

function TYXDXMLWriter.BeginWriteListItem(Index: Integer): Boolean;
begin
  BeginWrite(CSTR_COLLECTION_ITEM_TAG);
  Result := True;
end;

function TYXDXMLWriter.BeginWriteRoot(const ClsName: string): Boolean;
begin
  if Length(ClsName) > 0 then
    fRootObject := fDocument.AddChild(ClsName)
  else fRootObject := fDocument.AddChild('XMLObject');
  Result := True;
end;

constructor TYXDXMLWriter.Create(aInstance: TObject);
begin
  fDocument := NewXMLDocument;
  fDocument.Encoding := 'utf-8';
end;

destructor TYXDXMLWriter.Destroy;
begin
  inherited;
end;

procedure TYXDXMLWriter.EndWrite;
begin
  fRootObject := fRootObject.ParentNode;
end;

procedure TYXDXMLWriter.EndWriteList;
begin
  fRootObject := fRootObject.ParentNode;
end;

procedure TYXDXMLWriter.EndWriteListItem;
begin
  fRootObject := fRootObject.ParentNode;
end;

procedure TYXDXMLWriter.EndWriteRoot;
begin 
end;

procedure TYXDXMLWriter.SaveToStream(avOut: TStream);
begin
  fDocument.SaveToStream(avOut);
end;

procedure TYXDXMLWriter.SetObjectNode(const Value: IXMLNode);
begin
  fDocument := Value.OwnerDocument;
  fRootObject := Value;
end;

procedure TYXDXMLWriter.WriteAnsiChar(const Name: string; Value: AnsiChar);
begin
  fRootObject.AddChild(Name).Text := Value;
end;

procedure TYXDXMLWriter.WriteAnsiStr(const Name: string;
  const Value: AnsiString);
begin
  fRootObject.AddChild(Name).Text := Value;
end;

procedure TYXDXMLWriter.WriteAnsiStrListItem(Index: Integer;
  const Value: AnsiString);
begin
  fRootObject.AddChild(CSTR_COLLECTION_ITEM_TAG).Text := Value;
end;

procedure TYXDXMLWriter.WriteByte(const Name: string; Value: Byte);
begin
  fRootObject.AddChild(Name).Text := IntToStr(Value);
end;

procedure TYXDXMLWriter.WriteChar(const Name: string; Value: Char);
begin
  fRootObject.AddChild(Name).Text := Value;
end;

procedure TYXDXMLWriter.WriteFloat(const Name: string; Value: Extended);
begin
  fRootObject.AddChild(Name).Text := FloatToStr(Value);
end;

procedure TYXDXMLWriter.WriteInt64(const Name: string; Value: Int64);
begin
  fRootObject.AddChild(Name).Text := IntToStr(Value);
end;

procedure TYXDXMLWriter.WriteInteger(const Name: string; Value: Integer);
begin
  fRootObject.AddChild(Name).Text := IntToStr(Value);
end;

procedure TYXDXMLWriter.WritePointer(const Name: string; Buf: Pointer;
  Len: Integer);
var
  Buffer: AnsiString;
  Node: IXMLNode;
begin
  if (Len <= 0) then Exit;
  SetLength(Buffer, Len * 2);
  BinToHex(Buf, @Buffer[1], Len);
  Node := fDocument.CreateNode('Data');
  Node.ChildNodes.Add(fDocument.CreateNode(Buffer, ntCData));
  fRootObject.ChildNodes.Add(Node);
  SetLength(Buffer, 0);
end;

procedure TYXDXMLWriter.WriteString(const Name, Value: string);
begin
  fRootObject.AddChild(Name).Text := Value;
end;

procedure TYXDXMLWriter.WriteStrListItem(Index: Integer; const Value: string);
begin
  fRootObject.AddChild(CSTR_COLLECTION_ITEM_TAG).Text := Value;
end;

procedure TYXDXMLWriter.WriteWideChar(const Name: string; Value: WideChar);
begin
  fRootObject.AddChild(Name).Text := Value;
end;

procedure TYXDXMLWriter.WriteWideStr(const Name: string;
  const Value: WideString);
begin
  fRootObject.AddChild(Name).Text := Value;
end;

procedure TYXDXMLWriter.WriteWideStrListItem(Index: Integer;
  const Value: WideString);
begin
  fRootObject.AddChild(CSTR_COLLECTION_ITEM_TAG).Text := Value;
end;

procedure TYXDXMLWriter.WriteWord(const Name: string; Value: Word);
begin
  fRootObject.AddChild(Name).Text := IntToStr(Value);
end;

{ TYXDXMLReader }

const
  BASE64_ALPHABETS: array [0..64] of AnsiChar =
    ('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
   'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
   'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
   'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
   'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
   'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
   'w', 'x', 'y', 'z', '0', '1', '2', '3',
   '4', '5', '6', '7', '8', '9', '+', '/', '=');

   BASE64_ALPHABET_INDEXES: array [43..122] of Byte =
    (62, 255, 255, 255, 63, 52, 53, 54,
    55, 56, 57, 58, 59, 60, 61, 255,
    255, 255, 64, 255, 255, 255, 0, 1,
    2, 3, 4, 5, 6, 7, 8, 9,
    10, 11, 12, 13, 14, 15, 16, 17,
    18, 19, 20, 21, 22, 23, 24, 25,
    255, 255, 255, 255, 255, 255, 26, 27,
    28, 29, 30, 31, 32, 33, 34, 35,
    36, 37, 38, 39, 40, 41, 42, 43,
    44, 45, 46, 47, 48, 49, 50, 51);  

type
  FourBytes = array [0..3] of Byte;
  ThreeBytes = array [0..2] of Byte;
  PFourBytes = ^FourBytes;
  PThreeBytes = ^ThreeBytes;

function Base64Encode(Input: Pointer; InputSize: Integer;
  Output: Pointer): Integer;
var
  IPtr: PThreeBytes;
  OPtr: PFourBytes;
  IEnd: LongInt;
begin
  Result := (InputSize + 2) div 3 * 4;
  if Output = nil then Exit;
  IPtr := PThreeBytes(Input);
  OPtr := PFourBytes(Output);
  IEnd := LongInt(Input) + InputSize;
  while IEnd > Integer(IPtr) do
  begin
    OPtr[0] := IPtr[0] shr 2;
    OPtr[1] :=(IPtr[0] shl 4) and $30;
    if IEnd - Integer(IPtr) > 2 then
    begin
      OPtr[1] := OPtr[1] or(IPtr[1] shr 4);
      OPtr[2] :=(IPtr[1] shl 2) and $3f;
      OPtr[2] := OPtr[2] or IPtr[2] shr 6;
      OPtr[3] := IPtr[2] and $3f;
    end
    else if IEnd - Integer(IPtr) > 1 then
    begin
      OPtr[1] := OPtr[1] or(IPtr[1] shr 4);
      OPtr[2] :=(IPtr[1] shl 2) and $3f;
      OPtr[3] := 64;
    end
    else begin
      OPtr[2] := 64;
      OPtr[3] := 64;
    end;
    
    OPtr[0] := Ord(BASE64_ALPHABETS[OPtr[0]]);
    OPtr[1] := Ord(BASE64_ALPHABETS[OPtr[1]]);
    OPtr[2] := Ord(BASE64_ALPHABETS[OPtr[2]]);
    OPtr[3] := Ord(BASE64_ALPHABETS[OPtr[3]]);

    Inc(IPtr);
    Inc(OPtr);
  end;
end;

function Base64Encode(Input: Pointer; InputSize: Integer): AnsiString; overload;
begin
  SetLength(Result,(InputSize + 2) div 3 * 4);
  Base64Encode(Input, InputSize, Pointer(Result));
end;

function Base64Encode(const Input: AnsiString): AnsiString; overload;
begin
  SetLength(Result,(Length(Input) + 2) div 3 * 4);
  Base64Encode(Pointer(Input), Length(Input), Pointer(Result));
end;

function Base64Decode(Input: Pointer; InputSize: Integer;
  Output: Pointer): Integer;
var
  IPtr: PFourBytes;
  OPtr: PThreeBytes;
  i: Integer;
begin
  Result :=(InputSize div 4) * 3;
  if PAnsiChar(Input)[InputSize - 2] = '=' then Dec(Result, 2)
  else if PAnsiChar(Input)[InputSize - 1] = '=' then Dec(Result);
  if Output = nil then Exit;
  IPtr := PFourBytes(Input);
  OPtr := PThreeBytes(Output);
  for i := 1 to InputSize div 4 do 
  begin
    IPtr[0] := BASE64_ALPHABET_INDEXES[IPtr[0]];
    IPtr[1] := BASE64_ALPHABET_INDEXES[IPtr[1]];
    IPtr[2] := BASE64_ALPHABET_INDEXES[IPtr[2]];
    IPtr[3] := BASE64_ALPHABET_INDEXES[IPtr[3]];
    OPtr[0] :=(IPtr[0] shl 2) or(IPtr[1] shr 4);
    if(IPtr[2] <> 64) then
    begin
      OPtr[1] :=(IPtr[1] shl 4) or(IPtr[2] shr 2);
    end;

    if(IPtr[3] <> 64) then
    begin
      OPtr[2] :=(IPtr[2] shl 6) or IPtr[3];
    end;

    Inc(IPtr);
    Inc(OPtr);
  end;
end;

function Base64Decode(const Input: AnsiString;
  Output: Pointer): Integer; overload;
begin
  Result := Base64Decode(Pointer(Input), Length(Input), Output);
end;

function Base64Decode(const Input: AnsiString): AnsiString; overload;
var
  InputSize: Integer;
  OutputSize: Integer;
begin
  InputSize := Length(Input);
  if Input[InputSize] = #0 then Dec(InputSize);
  OutputSize :=(InputSize div 4) * 3;
  if Input[InputSize - 1] = '=' then Dec(OutputSize, 2)
  else if Input[InputSize] = '=' then Dec(OutputSize);
  SetLength(Result, OutputSize);
  Base64Decode(Pointer(Input), InputSize, Pointer(Result));
end;

function TYXDXMLReader.BeginRead(const Name: string): Boolean;
var
  ChildNode: IXMLNode;
begin
  ChildNode := fRootObject.ChildNodes.FindNode(Name);
  Result := Assigned(ChildNode);
  if Result then fRootObject := ChildNode;
end;

function TYXDXMLReader.BeginReadList(out Count: Integer): Boolean;
var
  ChildNode: IXMLNode;
begin
  ChildNode := fRootObject.ChildNodes.FindNode(CSTR_COLLECTION_TAG);
  if ChildNode = nil then Result := False
  else begin
    Count := ChildNode.ChildNodes.Count;
    if Count = 0 then Result := False
    else begin
      fRootObject := ChildNode.ChildNodes[0];  
      Result := True;
    end;
  end;
end;

function TYXDXMLReader.BeginReadListItem(Index: Integer): Boolean;
begin
  Result := Assigned(fRootObject);
end;

function TYXDXMLReader.BeginReadRoot(out ClsName: string): Boolean;
begin
  ClsName := fRootObject.NodeName;
  Result := True;
end;

constructor TYXDXMLReader.Create;
begin
end;

constructor TYXDXMLReader.Create(const Node: IXMLNode);
begin
  fDocument := Node.OwnerDocument;
  fRootObject := Node;
end;

destructor TYXDXMLReader.Destroy;
begin
  inherited;
end;

procedure TYXDXMLReader.EndRead;
begin
  fRootObject := fRootObject.ParentNode;
end;

procedure TYXDXMLReader.EndReadList;
begin
  fRootObject := fRootObject.ParentNode;
end;

function TYXDXMLReader.EndReadListItem: Boolean;
var
  Next: IXMLNode;
begin
  Next := fRootObject.NextSibling;
  if Next = nil then
  begin
    fRootObject := fRootObject.ParentNode;
    Result := False;
  end
  else begin
    fRootObject := fRootObject.NextSibling;
    Result := True;
  end;
end;

procedure TYXDXMLReader.LoadFromStream(avIn: TStream);
begin
  try
    fDocument := NewXMLDocument;
  except
    CoInitialize(nil);
    fDocument := NewXMLDocument;
  end;
  fDocument.LoadFromStream(avIn);
  fRootObject := fDocument.DocumentElement;
end;

function TYXDXMLReader.ReadAnsiChar(const Name: string;
  out Value: AnsiChar): Boolean;
var
  ChildNode: IXMLNode;
  text: AnsiString;
begin
  ChildNode := fRootObject.ChildNodes.FindNode(Name);
  if ChildNode = nil then Result := False
  else begin
    text := AnsiString(ChildNode.Text);
    if Length(text) = 1 then
    begin
      Value := text[1];
      Result := True;
    end
    else Result := False
  end;
end;

function TYXDXMLReader.ReadAnsiStr(const Name: string;
  out Value: AnsiString): Boolean;
var
  ChildNode: IXMLNode;
begin
  ChildNode := fRootObject.ChildNodes.FindNode(Name);
  if ChildNode = nil then Result := False
  else begin
    Value := ChildNode.Text;
    Result := True;
  end;
end;

function TYXDXMLReader.ReadAnsiStrListItem(Index: Integer;
  out Value: AnsiString; out More: Boolean): Boolean;
begin
  Value := fRootObject.Text;
  if fRootObject.NextSibling = nil then
  begin
    fRootObject := fRootObject.ParentNode;
    more := False;
  end
  else begin
    fRootObject := fRootObject.NextSibling;
    more := True;
  end;
  Result := True;
end;

function TYXDXMLReader.ReadByte(const Name: string; out Value: Byte): Boolean;
var
  ChildNode: IXMLNode;
  c: Integer;
begin
  ChildNode := fRootObject.ChildNodes.FindNode(Name);
  if ChildNode = nil then Result := False
  else begin
    Val(ChildNode.Text, Value, c);
    Result := c = 0;
  end;
end;

function TYXDXMLReader.ReadChar(const Name: string; out Value: Char): Boolean;
var
  ChildNode: IXMLNode;
  text: string;
begin
  ChildNode := fRootObject.ChildNodes.FindNode(Name);
  if ChildNode = nil then Result := False
  else begin
    text := ChildNode.Text;
    if Length(text) = 1 then begin
      Value := text[1];
      Result := True;
    end
    else Result := False
  end;
end;

function TYXDXMLReader.ReadFloat(const Name: string;
  out Value: Extended): Boolean;
var
  ChildNode: IXMLNode;
begin
  ChildNode := fRootObject.ChildNodes.FindNode(Name);
  if ChildNode = nil then Result := False
  else begin
    Value := StrToFloat(ChildNode.Text);
    Result := True;
  end;
end;

function TYXDXMLReader.ReadInt64(const Name: string; out Value: Int64): Boolean;
var
  ChildNode: IXMLNode;
begin
  ChildNode := fRootObject.ChildNodes.FindNode(Name);
  if ChildNode = nil then Result := False
  else begin
    Value := StrToInt64(ChildNode.Text);
    Result := True;
  end;
end;

function TYXDXMLReader.ReadInteger(const Name: string;
  out Value: Integer): Boolean;
var
  ChildNode: IXMLNode;
  c: Integer;
begin
  ChildNode := fRootObject.ChildNodes.FindNode(Name);
  if ChildNode = nil then Result := False
  else begin
    Val(ChildNode.Text, Value, c);
    Result := c = 0;
  end;
end;

function TYXDXMLReader.ReadPointer(const Name: string; Buf: Pointer;
  var Len: Integer): Boolean;
var
  ChildNode: IXMLNode;
  Buffer: string;
begin
  ChildNode := fRootObject.ChildNodes.FindNode('Data');
  if ChildNode = nil then Result := False
  else begin
    Buffer := ChildNode.Text;
    if Length(Buffer) <> Len * 2 then Result := False
    else begin
      HexToBin(@Buffer[1], Buf, Len);
      Result := True;
    end;
  end;
end;

function TYXDXMLReader.ReadString(const Name: string;
  out Value: string): Boolean;
var
  ChildNode: IXMLNode;
begin
  ChildNode := fRootObject.ChildNodes.FindNode(Name);
  if ChildNode = nil then Result := False
  else begin
    Value := ChildNode.Text;
    Result := True;
  end;
end;

function TYXDXMLReader.ReadStrListItem(Index: Integer; out Value: string;
  out More: Boolean): Boolean;
begin
  Value := fRootObject.Text;
  if fRootObject.NextSibling = nil then begin
    fRootObject := fRootObject.ParentNode;
    more := False;
  end else begin
    fRootObject := fRootObject.NextSibling;
    more := True;
  end;
  Result := True;
end;

function TYXDXMLReader.ReadWideChar(const Name: string;
  out Value: WideChar): Boolean;
var
  ChildNode: IXMLNode;
  text: WideString;
begin
  ChildNode := fRootObject.ChildNodes.FindNode(Name);
  if ChildNode = nil then Result := False
  else begin
    text := WideString(ChildNode.Text);
    if Length(text) = 1 then
    begin
      Value := text[1];
      Result := True;
    end
    else Result := False;
  end;
end;

function TYXDXMLReader.ReadWideStr(const Name: string;
  out Value: WideString): Boolean;
var
  ChildNode: IXMLNode;
begin
  ChildNode := fRootObject.ChildNodes.FindNode(Name);
  if ChildNode = nil then Result := False
  else begin
    Value := ChildNode.Text;
    Result := True;
  end;
end;

function TYXDXMLReader.ReadWideStrListItem(Index: Integer;
  out Value: WideString; out More: Boolean): Boolean;
begin
  Value := fRootObject.Text;
  if fRootObject.NextSibling = nil then begin
    fRootObject := fRootObject.ParentNode;
    more := False
  end else begin
    fRootObject := fRootObject.NextSibling;
    more := True;
  end;
  Result := True;
end;

function TYXDXMLReader.ReadWord(const Name: string; out Value: Word): Boolean;
var
  ChildNode: IXMLNode;
  c: Integer;
begin
  ChildNode := fRootObject.ChildNodes.FindNode(Name);
  if ChildNode = nil then Result := False
  else begin
    Val(ChildNode.Text, Value, c);
    Result := c = 0;
  end;
end;

procedure TYXDXMLReader.SetObjectNode(const Value: IXMLNode);
begin
  fDocument := Value.OwnerDocument;
  fRootObject := Value;
end;

{ TYXDJsonWriter }

function TYXDJsonWriter.BeginWrite(const Name: string): Boolean;
begin
  RootObject := RootObject.AddChild(Name);
  Result := RootObject <> nil;
end;

function TYXDJsonWriter.BeginWriteList(ListKind: TTypeKind;
  ItemCount: Integer): Boolean;
begin
  RootObject := RootObject.AddChild(CSTR_COLLECTION_TAG);
  Result := RootObject <> nil;
end;

function TYXDJsonWriter.BeginWriteListItem(Index: Integer): Boolean;
begin
  BeginWrite(CSTR_COLLECTION_ITEM_TAG);
  Result := True;
end;

function TYXDJsonWriter.BeginWriteRoot(const ClsName: string): Boolean;
begin
  if Length(ClsName) > 0 then
    RootObject := json.AddChild(ClsName)
  else RootObject := json.AddChild('JSONObject');
  Result := True;
end;

constructor TYXDJsonWriter.Create(aInstance: TObject);
begin
  json := TJSONObject.Create();
end;

destructor TYXDJsonWriter.Destroy;
begin
  FreeAndNil(json);
  inherited;
end;

procedure TYXDJsonWriter.EndWrite;
begin
  RootObject := RootObject.Parent;
end;

procedure TYXDJsonWriter.EndWriteList;
begin
  RootObject := RootObject.Parent;
end;

procedure TYXDJsonWriter.EndWriteListItem;
begin
  RootObject := RootObject.Parent;
end;

procedure TYXDJsonWriter.EndWriteRoot;
begin
end;

procedure TYXDJsonWriter.SaveToStream(avOut: TStream);
var
  text: string;
begin
  text := TJson.GenerateText(json);
  if Length(text) > 0 then  
    avOut.WriteBuffer(text[1], Length(text));
end;

procedure TYXDJsonWriter.WriteAnsiChar(const Name: string; Value: AnsiChar);
begin
  RootObject.Add(Name, Value);
end;

procedure TYXDJsonWriter.WriteAnsiStr(const Name: string;
  const Value: AnsiString);
begin
  RootObject.Add(Name, Value);
end;

procedure TYXDJsonWriter.WriteAnsiStrListItem(Index: Integer;
  const Value: AnsiString);
begin
  RootObject.Add(CSTR_COLLECTION_ITEM_TAG, Value);
end;

procedure TYXDJsonWriter.WriteByte(const Name: string; Value: Byte);
begin
  RootObject.Add(Name, Value);
end;

procedure TYXDJsonWriter.WriteChar(const Name: string; Value: Char);
begin
  RootObject.Add(Name, Value);
end;

procedure TYXDJsonWriter.WriteFloat(const Name: string; Value: Extended);
begin
  RootObject.Add(Name, Value);
end;

procedure TYXDJsonWriter.WriteInt64(const Name: string; Value: Int64);
begin
  RootObject.Add(Name, Value);
end;

procedure TYXDJsonWriter.WriteInteger(const Name: string; Value: Integer);
begin
  RootObject.Add(Name, Value);
end;

procedure TYXDJsonWriter.WritePointer(const Name: string; Buf: Pointer;
  Len: Integer);
var
  Buffer: AnsiString;
begin
  if (Len <= 0) then Exit;
  SetLength(Buffer, Len * 2);
  BinToHex(Buf, @Buffer[1], Len);
  RootObject.Add(Name, Buffer);
  SetLength(Buffer, 0);
end;

procedure TYXDJsonWriter.WriteString(const Name, Value: string);
begin
  RootObject.Add(Name, Value);
end;

procedure TYXDJsonWriter.WriteStrListItem(Index: Integer; const Value: string);
begin
  RootObject.Add(CSTR_COLLECTION_ITEM_TAG, Value);
end;

procedure TYXDJsonWriter.WriteWideChar(const Name: string; Value: WideChar);
begin
  RootObject.Add(Name, Value);
end;

procedure TYXDJsonWriter.WriteWideStr(const Name: string;
  const Value: WideString);
begin
  RootObject.Add(Name, Value);
end;

procedure TYXDJsonWriter.WriteWideStrListItem(Index: Integer;
  const Value: WideString);
begin
  RootObject.Add(CSTR_COLLECTION_ITEM_TAG, Value);
end;

procedure TYXDJsonWriter.WriteWord(const Name: string; Value: Word);
begin
  RootObject.Add(Name, Value);
end;

{ TYXDJsonReader }

function TYXDJsonReader.BeginRead(const Name: string): Boolean;
var
  ChildNode: TJSONObject;
begin
  ChildNode := RootObject.ChildNodes[Name];
  Result := Assigned(ChildNode);
  if not Result then begin
    ChildNode := RootObject.Parent;
    Result := Assigned(ChildNode);
    if Result then begin
      ChildNode := ChildNode.ChildNodes[Name];
      Result := Assigned(ChildNode);
      if (Result) then Inc(ref);      
    end;
  end;
  if Result then
    RootObject := ChildNode;
end;

function TYXDJsonReader.BeginReadList(out Count: Integer): Boolean;
var
  ChildNode: TJSONObject;
begin
  ChildNode := RootObject.ChildNodes[CSTR_COLLECTION_TAG];
  if ChildNode = nil then Result := False
  else begin
    Count := ChildNode.Count;
    if Count = 0 then Result := False
    else begin
      RootObject := ChildNode.ChildByIndex[0];  
      Result := True;
    end;
  end;
end;

function TYXDJsonReader.BeginReadListItem(Index: Integer): Boolean;
begin
  Result := Assigned(RootObject);
end;

function TYXDJsonReader.BeginReadRoot(out ClsName: string): Boolean;
begin
  ClsName := TJSONobjectmethod(RootObject.Parent).Name;
  Result := True;
end;

constructor TYXDJsonReader.Create(aInstance: TObject);
begin
  ref := 0;
end;

destructor TYXDJsonReader.Destroy;
begin
  FreeAndNil(json);  
  inherited;
end;

procedure TYXDJsonReader.EndRead;
begin
  RootObject := RootObject.Parent;
  if ref > 0 then begin
    dec(ref);
    RootObject := RootObject.Parent;
  end;
end;

procedure TYXDJsonReader.EndReadList;
begin
  RootObject := RootObject.Parent;
end;

function TYXDJsonReader.EndReadListItem: Boolean;
var
  Next: TJSONObject;
begin
  Next := RootObject.NextSibling;
  if Next = nil then begin
    RootObject := RootObject.Parent;
    Result := False;
  end else begin
    RootObject := Next;
    Result := True;
  end;
end;

procedure TYXDJsonReader.LoadFromStream(avIn: TStream);
var
  s: TStrings;
begin
  s := TStringList.Create;
  try
    s.LoadFromStream(avIn);
    json := TJSONObject(TJSON.ParseText(s.Text));
    if json.Count = 1 then begin
      RootObject := TJSONObject(json.Child[0]);
      if RootObject.Count = 0 then begin
        RootObject := TJSONObject(TJSONobjectmethod(RootObject).ObjValue);
      end;
    end else
      RootObject := json;
  finally
    s.Free;
  end;
end;

function TYXDJsonReader.ReadAnsiChar(const Name: string;
  out Value: AnsiChar): Boolean;
var
  ChildNode: TJSONObject;
  text: AnsiString;
begin
  ChildNode := RootObject.ChildNodes[Name];
  if ChildNode = nil then Result := False
  else begin
    text := AnsiString(ChildNode.Value);
    if Length(text) = 1 then begin
      Value := text[1];
      Result := True;
    end else Result := False
  end;
end;

function TYXDJsonReader.ReadAnsiStr(const Name: string;
  out Value: AnsiString): Boolean;
var
  ChildNode: TJSONObject; 
begin
  ChildNode := RootObject.ChildNodes[Name];
  if ChildNode = nil then Result := False
  else begin
    Value := ChildNode.Value;
    Result := True;
  end;
end;

function TYXDJsonReader.ReadAnsiStrListItem(Index: Integer;
  out Value: AnsiString; out More: Boolean): Boolean;
var
  Next: TJSONObject;
begin
  Value := RootObject.Value;
  Next := RootObject.NextSibling;
  if Next = nil then begin
    RootObject := RootObject.Parent;
    more := False;
  end else begin
    RootObject := Next;
    more := True;
  end;
  Result := True;
end;

function TYXDJsonReader.ReadByte(const Name: string; out Value: Byte): Boolean;
var
  ChildNode: TJSONObject; 
  c: Integer;
begin
  ChildNode := RootObject.ChildNodes[Name];
  if ChildNode = nil then Result := False
  else begin
    Val(ChildNode.Value, Value, c);
    Result := c = 0;
  end;
end;

function TYXDJsonReader.ReadChar(const Name: string; out Value: Char): Boolean;
var
  ChildNode: TJSONObject;
  text: string;
begin
  ChildNode := RootObject.ChildNodes[Name];
  if ChildNode = nil then Result := False
  else begin
    text := ChildNode.Value;
    if Length(text) = 1 then begin
      Value := text[1];
      Result := True;
    end else Result := False
  end;
end;

function TYXDJsonReader.ReadFloat(const Name: string;
  out Value: Extended): Boolean;
var
  ChildNode: TJSONObject; 
begin
  ChildNode := RootObject.ChildNodes[Name];
  if ChildNode = nil then Result := False
  else begin
    Value := StrToFloat(ChildNode.Value);
    Result := True;
  end;
end;

function TYXDJsonReader.ReadInt64(const Name: string;
  out Value: Int64): Boolean;
var
  ChildNode: TJSONObject; 
begin
  ChildNode := RootObject.ChildNodes[Name];
  if ChildNode = nil then Result := False
  else begin
    Value := StrToInt64(ChildNode.Value);
    Result := True;
  end;
end;

function TYXDJsonReader.ReadInteger(const Name: string;
  out Value: Integer): Boolean;
var
  ChildNode: TJSONObject;
  c: Integer;
begin
  ChildNode := RootObject.ChildNodes[Name];
  if ChildNode = nil then Result := False
  else begin
    Val(ChildNode.Value, Value, c);
    Result := c = 0;
  end;
end;

function TYXDJsonReader.ReadPointer(const Name: string; Buf: Pointer;
  var Len: Integer): Boolean;
var
  ChildNode: TJSONObject;
  Buffer: string;
begin
  ChildNode := RootObject.ChildNodes[Name];
  if ChildNode = nil then Result := False
  else begin
    Buffer := ChildNode.Value;
    if Length(Buffer) <> Len * 2 then Result := False
    else begin
      HexToBin(@Buffer[1], Buf, Len);
      Result := True;
    end;
  end;
end;

function TYXDJsonReader.ReadString(const Name: string;
  out Value: string): Boolean;
var
  ChildNode: TJSONObject;
begin
  ChildNode := RootObject.ChildNodes[Name];
  if ChildNode = nil then Result := False
  else begin
    Value := ChildNode.Value;
    Result := True;
  end;
end;

function TYXDJsonReader.ReadStrListItem(Index: Integer; out Value: string;
  out More: Boolean): Boolean;
var
  Next: TJSONObject;
begin
  Value := RootObject.Value;
  Next := RootObject.NextSibling;
  if Next = nil then begin
    RootObject := RootObject.Parent;
    more := False;
  end else begin
    RootObject := Next;
    more := True;
  end;
  Result := True;
end;

function TYXDJsonReader.ReadWideChar(const Name: string;
  out Value: WideChar): Boolean;
var
  ChildNode: TJSONObject;
  text: WideString;
begin
  ChildNode := RootObject.ChildNodes[Name];
  if ChildNode = nil then Result := False
  else begin
    text := WideString(ChildNode.Value);
    if Length(text) = 1 then begin
      Value := text[1];
      Result := True;
    end else Result := False
  end;
end;

function TYXDJsonReader.ReadWideStr(const Name: string;
  out Value: WideString): Boolean;
var
  ChildNode: TJSONObject;
begin
  ChildNode := RootObject.ChildNodes[Name];
  if ChildNode = nil then Result := False
  else begin
    Value := ChildNode.Value;
    Result := True;
  end;
end;

function TYXDJsonReader.ReadWideStrListItem(Index: Integer;
  out Value: WideString; out More: Boolean): Boolean;
var
  Next: TJSONObject;
begin
  Value := RootObject.Value;
  Next := RootObject.NextSibling;
  if Next = nil then begin
    RootObject := RootObject.Parent;
    more := False;
  end else begin
    RootObject := Next;
    more := True;
  end;
  Result := True;
end;

function TYXDJsonReader.ReadWord(const Name: string; out Value: Word): Boolean;
var
  ChildNode: TJSONObject;
  c: Integer;
begin
  ChildNode := RootObject.ChildNodes[Name];
  if ChildNode = nil then Result := False
  else begin
    Val(ChildNode.Value, Value, c);
    Result := c = 0;
  end;
end;

end.
