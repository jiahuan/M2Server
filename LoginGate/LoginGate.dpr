program LoginGate;

uses
  Forms,
  Main in 'Main.pas' {FrmMain},
  GateShare in 'GateShare.pas',
  GeneralConfig in 'GeneralConfig.pas' {frmGeneralConfig},
  IPaddrFilter in 'IPaddrFilter.pas' {frmIPaddrFilter},
  HUtil32 in '..\Common\HUtil32.pas',
  Grobal2 in '..\Common\Grobal2.pas',
  Common in '..\Common\Common.pas',
  GateCommon in '..\Common\GateCommon.pas',
  SafeConfig in 'SafeConfig.pas' {frmSafeConfig},
  DES in '..\Common\DES.pas',
  MyCommon in '..\MyCommon\MyCommon.pas',
  MakeLoginCommon in '..\Common\MakeLoginCommon.pas',
  EDcodeEx in '..\Common\EDcodeEx.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'LoginGate';
  Application.CreateForm(TFrmMain, FrmMain);
  Application.CreateForm(TfrmGeneralConfig, frmGeneralConfig);
  Application.CreateForm(TfrmIPaddrFilter, frmIPaddrFilter);
  Application.Run;
end.

