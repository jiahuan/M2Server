unit SafeConfig;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, IniFiles;

type
  TfrmSafeConfig = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    GroupBoxNet: TGroupBox;
    LabelGateIPaddr: TLabel;
    EditSoftVersionDate: TEdit;
    ButtonOK: TButton;
    TabSheet2: TTabSheet;
    Label2: TLabel;
    EdtPassword: TEdit;
    MemoWarningMsg: TMemo;
    Button1: TButton;
    procedure ButtonOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSafeConfig: TfrmSafeConfig;

implementation

uses HUtil32,GateShare;
{$R *.dfm}

procedure TfrmSafeConfig.ButtonOKClick(Sender: TObject);
var
  g_sPassWord,SoftVersionDate: string;
//  boSpecLogin: Boolean;
  sPassWord: string;
  Conf: TIniFile;
begin
  SoftVersionDate := Trim(EditSoftVersionDate.Text);
  if SoftVersionDate <> '' then begin
    Application.MessageBox('�ͻ��˰�����ô��󣡣���', '������Ϣ', MB_OK + MB_ICONERROR);
    EditSoftVersionDate.SetFocus;
    Exit;
  end;

  //boSpecLogin := CheckBoxSpecLogin.Checked;  //20080831
  sPassWord := Trim(EdtPassword.Text); //20080831

  g_sPassWord := sPassWord;
//  g_boSpecLogin := boSpecLogin;
  g_SoftVersionDate := SoftVersionDate;
  if g_boSpecLogin then begin
    MemoWarningMsg.Lines.SaveToFile('.\WarningMsg.txt');
  end;
  
  Conf := TIniFile.Create('.\Config.ini');
  Conf.WriteString(GateClass, 'PassWord', g_sPassWord);
  Conf.WriteBool(GateClass, 'SpecLogin', g_boSpecLogin);
  Conf.WriteString(GateClass, 'BuildVersion', g_SoftVersionDate);
  Conf.Free;
  Close;
end;

end.
